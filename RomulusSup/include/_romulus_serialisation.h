#ifndef _ROMULUS_SERIALISATION_H
#define _ROMULUS_SERIALISATION_H

#include "romulus_internals.h"

#ifndef _ROMULUS_STRUCT_FIELD_NAME_MAX_LENGTH
    #define _ROMULUS_STRUCT_FIELD_NAME_MAX_LENGTH 256
#else /* _ROMULUS_STRUCT_FIELD_NAME_MAX_LENGTH */
    #error
#endif /* _ROMULUS_STRUCT_FIELD_NAME_MAX_LENGTH */

#ifndef _ROMULUS_STRUCT_MAX_LINE_LENGTH
    #define _ROMULUS_STRUCT_MAX_LINE_LENGTH 1024
#else /* _ROMULUS_STRUCT_MAX_LINE_LENGTH */
    #error
#endif /* _ROMULUS_STRUCT_MAX_LINE_LENGTH */

int _romulus_write_uint64_t(FILE* fd, const char* const parameter_name, const uint64_t parameter_value);
int _romulus_read_uint64_t(FILE* fd, const char* const parameter_name, uint64_t* const parameter_value_ptr);

int _romulus_write_int64_t(FILE* fd, const char* const parameter_name, const int64_t parameter_value);
int _romulus_read_int64_t(FILE* fd, const char* const parameter_name, int64_t* const parameter_value_ptr);

int _romulus_write_uint32_t(FILE* fd, const char* const parameter_name, const uint32_t parameter_value);
int _romulus_read_uint32_t(FILE* fd, const char* const parameter_name, uint32_t* const parameter_value_ptr);

int _romulus_write_int32_t(FILE* fd, const char* const parameter_name, const int32_t parameter_value);
int _romulus_read_int32_t(FILE* fd, const char* const parameter_name, int32_t* const parameter_value_ptr);

int _romulus_write_uint16_t(FILE* fd, const char* const parameter_name, const uint16_t parameter_value);
int _romulus_read_uint16_t(FILE* fd, const char* const parameter_name, uint16_t* const parameter_value_ptr);

int _romulus_write_int16_t(FILE* fd, const char* const parameter_name, const int16_t parameter_value);
int _romulus_read_int16_t(FILE* fd, const char* const parameter_name, int16_t* const parameter_value_ptr);

int _romulus_write_uint8_t(FILE* fd, const char* const parameter_name, const uint8_t parameter_value);
int _romulus_read_uint8_t(FILE* fd, const char* const parameter_name, uint8_t* const parameter_value_ptr);

int _romulus_write_int8_t(FILE* fd, const char* const parameter_name, const int8_t parameter_value);
int _romulus_read_int8_t(FILE* fd, const char* const parameter_name, int8_t* const parameter_value_ptr);

int _romulus_write_float(FILE* fd, const char* const parameter_name, const float parameter_value);
int _romulus_read_float(FILE* fd, const char* const parameter_name, float* const parameter_value_ptr);

int _romulus_write_bool(FILE* fd, const char* const parameter_name, const bool parameter_value);
int _romulus_read_bool(FILE* fd, const char* const parameter_name, bool* const parameter_value_ptr);

int _romulus_write_char_array(FILE* fd, const char* const parameter_name, const uint8_t parameter_value[], const unsigned int arrayLength);
int _romulus_read_char_array(FILE* fd, const char* const parameter_name, uint8_t* const parameter_value, const unsigned int arrayLength);

int _romulus_write_bool_array(FILE* fd, const char* const parameter_name, const bool (*const parameter_value)[size_of_type(romulus_params_t, Coefficients[0]) / size_of_type(romulus_params_t, Coefficients[0][0])], const unsigned int dim0, const unsigned int dim1);
int _romulus_read_bool_array(FILE* fd, const char* const parameter_name, bool (*const parameter_value)[size_of_type(romulus_params_t, Coefficients[0]) / size_of_type(romulus_params_t, Coefficients[0][0])], const unsigned int dim0, const unsigned int dim1);

#endif /* _ROMULUS_SERIALISATION_H */
