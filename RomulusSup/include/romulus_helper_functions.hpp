#ifndef ROMULUS_HELPER_FUNCTIONS_HPP
#define ROMULUS_HELPER_FUNCTIONS_HPP

#ifdef __cplusplus

extern "C" {
#include "romulus.h"
#include "romulus_frame.h"
#include "romulus_hist.h"
}

inline bool operator==(romulus_log_level_t const& a, romulus_log_level_t const& b)
{
    return a.level == b.level;
}

inline bool operator!=(romulus_log_level_t const& a, romulus_log_level_t const& b)
{
    return a.level != b.level;
}

inline bool operator==(romulus_type_info_type_wrapper_t const& a, romulus_type_info_type_wrapper_t const& b)
{
    return a.type == b.type;
}

inline bool operator!=(romulus_type_info_type_wrapper_t const& a, romulus_type_info_type_wrapper_t const& b)
{
    return a.type != b.type;
}

inline bool operator==(romulus_param_protection_t const& a, romulus_param_protection_t const& b)
{
    return a.protection == b.protection;
}

inline bool operator!=(romulus_param_protection_t const& a, romulus_param_protection_t const& b)
{
    return a.protection != b.protection;
}

inline bool operator==(romulus_response_code_t const& a, romulus_response_code_t const& b)
{
    return a.code == b.code;
}

inline bool operator!=(romulus_response_code_t const& a, romulus_response_code_t const& b)
{
    return a.code != b.code;
}

inline bool operator==(romulus_command_code_t const& a, romulus_command_code_t const& b)
{
    return a.command_code == b.command_code;
}

inline bool operator!=(romulus_command_code_t const& a, romulus_command_code_t const& b)
{
    return a.command_code != b.command_code;
}

inline bool operator==(romulus_hist_data_flag_t const& a, romulus_hist_data_flag_t const& b)
{
    return a.flag == b.flag;
}

inline bool operator!=(romulus_hist_data_flag_t const& a, romulus_hist_data_flag_t const& b)
{
    return a.flag != b.flag;
}

#endif

#endif /* ROMULUS_HELPER_FUNCTIONS_HPP */
