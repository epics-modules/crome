
/* Copyright (C) 1991-2016 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */




/* This header is separate from features.h so that the compiler can
   include it implicitly at the start of every compilation.  It must
   not itself include <features.h> or any other header that includes
   <features.h> because the implicit include comes before any feature
   test macros that may be defined in a source file before it first
   explicitly includes a system header.  GCC knows the name of this
   header in order to preinclude it.  */

/* glibc's intent is to support the IEC 559 math functionality, real
   and complex.  If the GCC (4.9 and later) predefined macros
   specifying compiler intent are available, use them to determine
   whether the overall intent is to support these features; otherwise,
   presume an older compiler has intent to support these features and
   define these macros by default.  */
/* wchar_t uses Unicode 8.0.0.  Version 8.0 of the Unicode Standard is
   synchronized with ISO/IEC 10646:2014, plus Amendment 1 (published
   2015-05-15).  */


/* We do not support C11 <threads.h>.  */

/** @brief Struct for historic data which is not used by the supervision. @ingroup romulus_structs */ 
typedef struct _romulus_packed {

int64_t TimeStamp; /**< Timestamp in miliseconds since POSIX epoch. */
int32_t MSID; /**< Same as romulus_params_t#MSID. */

float RawValue; /**< Same as romulus_rtmeas_t#RawValue. */
float Temperature2; /**< Same as romulus_rtmeas_t#Temperature2. */
float CaseTemp; /**< Same as romulus_rtmeas_t#CaseTemp. */

float Minus5va; /**< -5V analogue. */
float Minus15va; /**< -15V analogue. */
float Minus12va; /**< -12V analogue. */
float Minus2v5a; /**< -2,5V analogue. */

float Plus15va; /**< 15V analogue. */
float Plus5vZynq; /**< -5V for Zynq. */
float Plus3v3d; /**< 3V digital. */
float Plus5vd; /**< 5V digital. */
float Plus12vHV; /**< 12V for high voltage. */
float Plus12va; /**< 12V analogue. */
float Plus5va; /**< 5V analogue. */
float VRefHV; /**< Reference voltage for high voltage. */
float VRefComp; /**< Reference voltage for comparator. */
float vRefADCMeas; /**< Reference voltage for ADC measurments. */

int32_t LowVoltageFaults; /**< Low voltage faults packet in one hot encoding. */

bool SensorConnection; /**< Same as romulus_rtmeas_t#SensorConnection. */
bool PowerSupply; /**< Same as romulus_rtmeas_t#PowerSupply. */
bool Battery; /**< Same as romulus_rtmeas_t#Battery. */
bool Charger; /**< Same as romulus_rtmeas_t#Charger. */

bool Input1; /**< Same as romulus_rtmeas_t#Input1. */
bool Input2; /**< Same as romulus_rtmeas_t#Input2. */
bool Input3; /**< Same as romulus_rtmeas_t#Input3. */
bool Input4; /**< Same as romulus_rtmeas_t#Input4. */
bool Output1; /**< Same as romulus_rtmeas_t#Output1. */
bool Output2; /**< Same as romulus_rtmeas_t#Output2. */
bool Output3; /**< Same as romulus_rtmeas_t#Output3. */
bool Output4; /**< Same as romulus_rtmeas_t#Output4. */

} romulus_hist_int_t;
