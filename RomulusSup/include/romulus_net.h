#ifndef ROMULUS_NET_H_
#define ROMULUS_NET_H_

#include "romulus_frame.h"

#include <netdb.h>

/** @defgroup net_types Type definitions for romulus frame based network connections
 *  All type definitions for romulus frame based network connections.
 *  @{
 */

/**
 * @brief romulus_socket_t Romulus socket type. Does nothing really, it just helps readability.
 */
typedef int romulus_socket_t;

/**
 * @brief Internal type when a host tries to resolve another host to connect to it.
 */
typedef struct _resolve_return_t{
    struct sockaddr addr; /**< Socket address information. */
    socklen_t addr_len; /**< Size of the sockaddr struct */
} _resolve_return_t;

/**
 * @brief Internal data structure to store information about partially received frame on a per socket basis.
 */
typedef struct _romulus_net_frame_fragment_info_t{
    romulus_frame_t* frame_fragment; /**< Pointer to the frame fragment. */
    size_t frame_fragment_size; /**< Number of bytes that were already received of the frame. Max ETHERNET_MTU */
    romulus_socket_t sock; /**< Socket on which the frame fragment is being/was received */
} _romulus_net_frame_fragment_info_t;

/**
 * @brief Data structure to collect frame fragment information for multiple sockets. This type should
 * never be used directly but always thourgh its corresponding functions.
 */
typedef struct romulus_net_transmission_control_t{
    _romulus_net_frame_fragment_info_t* frame_fragments; /**< Array of frame fragment information of length num_frame_fragments. */
    unsigned int num_frame_fragments; /**< Length of the frame_fragments array. */
} romulus_net_transmission_control_t;
/** @} */

/** @defgroup net_connect Romulus frame connection lifecycle functions
 *  All function required to manage the lifecycle of connections to transmit romulus frames
 *  @{
 */

/**
 * @brief _romulus_resolve Tries to resolve a host name and port to return a sockaddr struct which can be used to connect to a host. This function
 * should not be used directly but it is used internally by romulus_net_connect(const char* const, const uint16_t).
 * @param host Name of the host, which can be in IP format (192.168.1.1) or URL format (cern.ch).
 * @param port Port number where the host should be connected.
 * @return On error, returns a _resolve_return_t where the addr_len field is zero. On success, returns a valid sockaddr struct/socklen_t pair.
 */
_resolve_return_t _romulus_resolve(const char* const host, const uint16_t port);
/**
 * @brief romulus_net_listen Creates a socket to listen on incoming connection on a specific port.
 * @param port The port number where the host should listen for incoming connections.
 * @return Returns -1 on error or a valid romulus socket on success (which is >= 0).
 */
romulus_socket_t romulus_net_listen(const uint16_t port);
/**
 * @brief romulus_net_accept Check if there are new incoming connections on a listening port and returns a valid romulus socket if there was
 * successfully found one.
 * @param listen_sock Romulus socket which is listening on incoming connections.
 * @return Returns -1 on error, listen_sock if no new incoming connection was found (this is legit as file descriptors are unique per process)
 * or a new valid romulus socket >= 0 if a external host succesfully connected.
 */
romulus_socket_t romulus_net_accept(romulus_socket_t listen_sock);
/**
 * @brief romulus_net_connect Connects to a remote host with a specific name on a specific port.
 * @param host Name of the host to which one wants to connect, which can be in IP format (192.168.1.1) or URL format (cern.ch).
 * @param port Port on which to try to connect on the remote host.
 * @return Returns -1 on error or a valid romulus socket which is greater or equal to zero.
 */
romulus_socket_t romulus_net_connect(const char* const host, const uint16_t port);
/**
 * @brief romulus_net_close Closes an open romulus socket.
 * @param control Pointer to the transmission control structure which was used when the socket was receiving data. This can be set to NULL if and
 * only if all reception on that socket was 1. done in blocking mode and 2. every call to
 * romulus_net_recv(romulus_net_transmission_control_t* const, romulus_socket_t, const bool, bool* const) was done with setting the romulus_net_transmission_control_t
 * pointer to NULL as well. Failing to comply to this can cause memory leaks or incorrect frame reception on future incoming connections.
 * The control struct stays valid after the function call and can be continued to be used. It can even lead to a memory leak or broken connections to call
 * romulus_net_init_transmission_control(void) before all other connections monitured by the control struct have been closed.
 * @param sock Romulus socket which has to be closed.
 * @return Returns -1 as a new (invalid) file descriptor as the input file descriptor has been invalidated.
 */
romulus_socket_t romulus_net_close(romulus_net_transmission_control_t* const control, romulus_socket_t sock);
/** @} */

/** @defgroup net_send_recv Romulus frame transmission and reception functions
 *  All function required to send and receive romulus frames
 *  @{
 */

/**
 * @brief romulus_net_send Sends a frame fragment/complete frame through a romulus socket.
 * @param frame_fragment Pointer to the frame/frame fragment to be sent. If a frame has already been sent partially and the rest of the frame has
 * to be sent, then the pointer needs to be set to the point from where on the rest of the frame has to be sent
 * @param frame_fragment_size Size of the frame/frame fragment to be sent. To get the size of a full frame, you may use
 * romulus_frame_get_size(const romulus_frame_t* const).
 * @param sock Romulus socket through which the romulus frame shall be sent.
 * @param block True if the sending should block or false if the sending should not be blocking.
 * @return Returns -1 on error or the number of bytes sent. If a frame was sent incompletely, it is up to the caller to either retry to send
 * the remaining bytes of the frame or to close the connection using romulus_net_close(romulus_net_transmission_control_t* const, romulus_socket_t)
 * as the connection is now in a dirty state.
 */
ssize_t romulus_net_send(const romulus_frame_t* const frame_fragment, const size_t frame_fragment_size, romulus_socket_t sock, bool block);
/**
 * @brief romulus_net_init_transmission_control Initialises a romulus net transmission control structure.
 * @return Returns an initialised romulus net transmission control structure.
 */
romulus_net_transmission_control_t romulus_net_init_transmission_control(void);
/**
 * @brief _romulus_net_recv_fragments A helper function to receive frame fragments. It should not be used directly and is called internally by
 * romulus_net_recv(romulus_net_transmission_control_t* const, romulus_socket_t, const bool, bool* const).
 * @param info Pointer to the fragment information structure specific to the used socket for reception.
 * @return Returns -1 on error, 0 if nothing or only a romulus frame fragment was received or 1 if a full frame was received.
 */
int _romulus_net_recv_fragments(_romulus_net_frame_fragment_info_t* const info);
/**
 * @brief romulus_net_recv Tries to receive a romulus frame on a specific socket.
 * @param control Pointer to the transmission control structure. This can be NULL if the reception is done in blocking mode. but a certain socket
 * must always be used either with a transmission control structure or without, e.g. if blocking and non blocking reads are done then a transmission
 * control structure has always to be used. In short, do not mix calling this function with NULL and a valid pointer on the same socket.
 * @param sock Romulus socket on which shall be used to receive a romulus frame.
 * @param block True if the call should block until a full frame has been received or false for non-blocking operation.
 * @param socket_error Pointer to a boolean, which will be set to true if the socket has to be close due to an error or false if the socket is still
 * functional after the call (even if NULL is returned).
 * @return Returns NULL on error or when no complete frame was received or a pointer to a valid (completely received) frame. A non-NULL return pointer has to
 * be freed a again with romulus_frame_free(romulus_frame_t* const). If NULL is returned, socket_error must be checked to see if there was an error which
 * causes that the socket has to be closed. If only a partial frame was received in non-blocking mode, NULL is returned and the partial frame is stored
 * using the tranmission control structure.
 */
romulus_frame_t* romulus_net_recv(romulus_net_transmission_control_t* const control, romulus_socket_t sock, const bool block, bool* const socket_error);
/**
 * @brief romulus_net_tcp_num_lost_packets Checks the number of lost packets for a socket which is a TCP connection.
 * @param sock Socket for which the number of lost packets is queried. This socket must be a TCP connection.
 * @return Returns a negative number (-1 usually) on error or the number of lost packets (which is greater or equal to zero).
 * Other numbers than -1 indicate an internal overflow and -1 cannot be distinguished from 2^32-1.
 */
int romulus_net_tcp_num_lost_packets(romulus_socket_t sock);
/**
 * @brief romulus_net_tcp_num_retrans_packets Checks the number of packets which are being retransmitted for a socket which is a TCP connection.
 * @param sock Socket for which the number of packets being retransmitted is queried. This socket must be a TCP connection.
 * @return Returns a negative number (-1 usually) on error or the number of packets which are being retransmitted (which is greater or equal to zero).
 * Other numbers than -1 indicate an internal overflow and -1 cannot be distinguished from 2^32-1.
 */
int romulus_net_tcp_num_retrans_packets(romulus_socket_t sock);
/** @} */

/** @defgroup net_transmission_control Romulus frame transmission control functions
 *  All function required to control the sending and reception of romulus frames
 *  @{
 */

/**
 * @brief _romulus_net_transmission_control_contains_element Checks if the transmission control structure already tracks a certain socket.
 * @param control Pointer to the transmission control structure.
 * @param sock Socket which has to be checked if it is already tracked by the transmission control structure.
 * @return Returns -1 on error, 0 if the socket is not being tracked and 1 if the socket is already in the transmission control structure.
 */
int _romulus_net_transmission_control_contains_element(romulus_net_transmission_control_t* const control, const romulus_socket_t sock);
/**
 * @brief _romulus_net_transmission_control_get_element Gets a frame fragment info struct for a specific socket in a transmission control structure.
 * @param control Pointer to the transmission control struct which has to be searched for a specific romulus socket.
 * @param sock Romulus socket which has to be searched for in the transmission control struct.
 * @return Return NULL on error (or if the socket is not being tracked in the transmission control struct) or a valid pointer to the frame fragment information
 * struct withing the transmission control struct.
 */
_romulus_net_frame_fragment_info_t* _romulus_net_transmission_control_get_element(romulus_net_transmission_control_t* const control, const romulus_socket_t sock);
/**
 * @brief _romulus_net_transmission_control_add_element Adds an element (e.g. a romulus socket) to the transmission control structure.
 * @param control Pointer to the transmission control structure to which the element has to be added.
 * @param sock Romulus socket which has to be added to the control structure.
 * @return Returns NULL on error or a pointer to the transmission control structure on success.
 */
romulus_net_transmission_control_t* _romulus_net_transmission_control_add_element(romulus_net_transmission_control_t* const control, const romulus_socket_t sock);
/**
 * @brief _romulus_net_transmission_control_remove_element Removes an element (e.g. a romulus socket plus the corresponding frame fragments) from
 * the transmission control structure.
 * @param control Pointer to the transmission control structure to which the element has to be removed.
 * @param sock Romulus socket which has to be removed from the control structure.
 * @return Returns NULL on error or a pointer to the transmission control structure on success.
 */
romulus_net_transmission_control_t* _romulus_net_transmission_control_remove_element(romulus_net_transmission_control_t* const control, const romulus_socket_t sock);
/** @} */

#endif /* ROMULUS_NET_H_ */
