#ifndef ROMULUS_HIST_H
#define ROMULUS_HIST_H

#include "romulus.h"

/** @defgroup romulus_hist All definition concerning historic data and events.
 * All type and function definition which are used for historic data and events.
 * @{
 */

/**
 * @brief The romulus_hist_data_flag struct wraps the flag to distinguish between normal, minute and hour values historic data.
 *
 * This data structure is used to have a typsafe way of handling enums and replaces enums when used in conjunction with
 * the defines ROMULUS_HIST_DATA_FLAG_MEASUREMENT, ROMULUS_HIST_DATA_FLAG_MINUTE and ROMULUS_HIST_DATA_FLAG_HOUR.
 */
typedef struct{
    int flag; /**< The actual data which is encapsulated by the struct */
} romulus_hist_data_flag_t;

/**
 * @brief Typesafe enum which indicates that the historic data flag with this was evaluated over a measurement period.
 *
 * For a more detailed description, look at @ref romulus_hist_data_flag_t.
 */
#define ROMULUS_HIST_DATA_FLAG_MEASUREMENT  ((romulus_hist_data_flag_t){ROMULUS_DATA_MINUTE_HOUR_RAW})
/**
 * @brief Typesafe enum which indicates that the historic data flag with this was evaluated over a minute.
 *
 * For a more detailed description, look at @ref romulus_hist_data_flag_t.
 */
#define ROMULUS_HIST_DATA_FLAG_MINUTE       ((romulus_hist_data_flag_t){ROMULUS_DATA_MINUTE_HOUR_MINUTE})
/**
 * @brief Typesafe enum which indicates that the historic data flag with this was evaluated over an hour.
 *
 * For a more detailed description, look at @ref romulus_hist_data_flag_t.
 */
#define ROMULUS_HIST_DATA_FLAG_HOUR         ((romulus_hist_data_flag_t){ROMULUS_DATA_MINUTE_HOUR_HOUR})

/**
 * @brief The romulus_hist_data_latched_data struct groups all historic kinds of data together that are latched/evaluated
 * over a different timespans (measurment times, minutes, hours).
 *
 * This struct is used to keep track of the different signals (alarms, faults and measurment values) over different
 * timespans and to provied a common interface to these similiar measurments.
 */
typedef struct{
    bool Alert; /**< Flag if there was an alert. */
    bool Alarm; /**< Flag if there was an alarm.*/
    bool SpecialAlarm; /**< Flag if there was a special alarm. */
    bool MinorFault; /**< Flag if there was a minor fault. */
    bool MajorFault; /**< Flag if there was a major fault. */
    bool PersistentFault; /**< Flag if there was a persistant fault. */

    float value; /**< Actual value of the measurment (can be base-, minute- or hour-value). */
    romulus_hist_data_flag_t flag; /**< Indicates over which timespan (measurment time, minute or hour) the other fields of the stuct were evaluated. */
} romulus_hist_data_latched_data_t;

/**
 * @brief romulus_hist_updated_latched_data Updates the boolean fiels of the romulus_hist_data_latched_data_t data structure.
 * @param status Reference to the current status of the system from which the data is taken.
 * @param value The current value (its interpretation depends of romulus_hist_data_latched_data_t#flag. This value just overwrites the current value in the struct.
 * @param flag The flag to indicate the interpretation of the complete struct (over which time frame the data is latched).
 * @param reset_latching If true, resets the latch to just take the value from status. If false, any boolean value in latched_data which is true stays true even if the same value in status is false.
 * @param latched_data Reference to the latched data struct which is updated.
 * @return Returns NULL on error and a valid pointer to latched_data on success.
 */
romulus_hist_data_latched_data_t* romulus_hist_updated_latched_data(const romulus_status_t* const status, const float value, const romulus_hist_data_flag_t flag,
                                                                    const bool reset_latching, romulus_hist_data_latched_data_t* const latched_data);

/**
 * @brief romulus_hist_data_create This function creates a historic data struct.
 * @param status Pointer to the romulus status struct.
 * @param rtmeas Pointer to the romulus real-time measurments struct.
 * @param latched_data Pointer to the latched data struct.
 * @param high_voltage Value of the high voltage.
 * @param hist_data Pointer to the memory location where to put the historic data struct.
 * @return Returns NULL on error or a pointer to the historic data struct just created.
 */
romulus_hist_data_t* romulus_hist_data_create(const romulus_status_t* const status, const romulus_rtmeas_t* const rtmeas,
                                              const romulus_hist_data_latched_data_t* const latched_data, const float high_voltage,
                                              romulus_hist_data_t* const hist_data);
/**
 * @brief romulus_hist_events_create This function is called muliple times (until it returns NULL) to create zero or more historic event structs.
 * @param msid Measurment ID of the device.
 * @param status Reference to the romulus status struct which will then be parsed by repetitive calls to create historic event structs.
 * @param hist_event Reference to the historic event struct which can be filled by calling this function.
 * @return If there were no changes in any of the boolean values of the romulus status struct compared to the romulus status struct of the previous
 * function call, the call will return NULL. If there were changes, the historic event struct is filled with data and a pointer to it is returned
 * and the functionhas to be called again until it returns NULL.
 */
romulus_hist_event_t* romulus_hist_events_create(const int32_t msid, const romulus_status_t* const status, romulus_hist_event_t* const hist_event);
/** @} */

#endif /* ROMULUS_HIST_H */
