#ifndef ROMULUS_H_
#define ROMULUS_H_

#include <stdint.h>
#include <sys/types.h>
#include <stdbool.h>
#include <stddef.h>

/* Define static assert for C++ compability */
#ifndef static_assert
    #include "assert.h"
#endif /* static_assert */

/** @defgroup romulus_structs All the different romulus structs.
 * Contains the definition of all the different structures which are used
 * by the ROMULUS library.
 * @{
 */

/**
 * @brief Defining packed attribute to have packed structs.
 */
#define _romulus_packed __attribute__ ((__packed__))

#include "_romulus_params_struct.h"

/**
 * @brief IG5-A20 CENTRONIC ionisation chamber.
 */
#define ROMULUS_DATA_DETECTOR_TYPE_IG5_A20 0
/**
 * @brief IG5-H20 CENTRONIC ionisation chamber.
 */
#define ROMULUS_DATA_DETECTOR_TYPE_IG5_H20 1
/**
 * @brief IG5T-A15 CENTRONIC ionisation chamber.
 */
#define ROMULUS_DATA_DETECTOR_TYPE_IG5T_A15 2
/**
 * @brief IG5T-N15 CENTRONIC ionisation chamber.
 */
#define ROMULUS_DATA_DETECTOR_TYPE_IG5T_N15 3
/**
 * @brief IG32-A3.1 CENTRONIC ionisation chamber.
 */
#define ROMULUS_DATA_DETECTOR_TYPE_IG32_A3_1 4
/**
 * @brief T32006 PTW ionisation chamber.
 */
#define ROMULUS_DATA_DETECTOR_TYPE_T32006 5
/**
 * @brief FHT762 REM counter, Thermo Scientific.
 */
#define ROMULUS_DATA_DETECTOR_TYPE_FHT762 6

/**
 * @brief In off mode, the device is powered, high voltage supplied, but no measurment data is established and no alarms are triggered and all outputs are in a safe state.
 */
#define ROMULUS_DATA_MODE_OFF 0
/**
 * @brief In maintainance mode the device is fully functional and all data is tagged as maintainance mode.
 */
#define ROMULUS_DATA_MODE_MAINTAINANCE 1
/**
 * @brief In measurment mode the device is fully functional and all data is tagged as measurment mode.
 */
#define ROMULUS_DATA_MODE_MEASUREMENT 2
/**
 * @brief In calibration mode, the device is in a specific mode for calibration for temperature compensation.
 */
#define ROMULUS_DATA_MODE_CALIBRATION_TEMPERATURE_COMPENSATION 3

/**
 * @brief Value to set the measurment unit to Sv/h.
 */
#define ROMULUS_DATA_MEASUREMENT_UNIT_SV_H 1
/**
 * @brief Value to set the measurment unit to Gy/h.
 */
#define ROMULUS_DATA_MEASUREMENT_UNIT_GY_H 2

/**
 * @brief Value to set the conversion factor to (Sv/h)/A.
 */
#define ROMULUS_DATA_CONVERSION_UNIT_MeasUnit_A 1
/**
 * @brief Value to set the conversion factor to (Sv/h)/cps.
 */
#define ROMULUS_DATA_CONVERSION_UNIT_MeasUnit_CPS 2

/**
 * @brief Value to set the prefix to nano.
 */
#define ROMULUS_DATA_DEFAULT_PREFIX_NANO 0
/**
 * @brief Value to set the prefix to micro.
 */
#define ROMULUS_DATA_DEFAULT_PREFIX_MICRO 1
/**
 * @brief Value to set the prefix to mili.
 */
#define ROMULUS_DATA_DEFAULT_PREFIX_MILI 2
/**
 * @brief Value to not use any prefix.
 */
#define ROMULUS_DATA_DEFAULT_PREFIX_NONE 3
/**
 * @brief Value to let the system automatically pick the prefix.
 */
#define ROMULUS_DATA_DEFAULT_PREFIX_AUTOMATIC 4

/**
 * @brief Value to set the averaging algorithm to simple moving averaging.
 */
#define ROMULUS_DATA_AVERAGING_ALGORITHM_SIMPLE_MOVING_AVERAGE 0
/**
 * @brief Value to set the averaging algorithm to exponential moving averaging.
 */
#define ROMULUS_DATA_AVERAGING_ALGORITHM_EXPONENTIAL_MOVING_AVERAGE 1
/**
 * @brief Value to set the averaging algorithm to weighted moving averaging.
 */
#define ROMULUS_DATA_AVERAGING_ALGORITHM_WEIGHTED_MOVING_AVERAGE 2

/**
 * @brief Value to pick the comparator operator bigger or equal.
 */
#define ROMULUS_DATA_OPERATOR_BIGGER_OR_EQUAL false
/**
 * @brief Value to pick the comparator operator smaller.
 */
#define ROMULUS_DATA_OPERATOR_SMALLER true

/**
 * @brief Bit mask to select or set output 1 for simulation.
 */
#define ROMULUS_DATA_SIM_MATRIX_OUTPUT_1_MASK 0x00000001
/**
 * @brief Bit mask to select or set output 2 for simulation.
 */
#define ROMULUS_DATA_SIM_MATRIX_OUTPUT_2_MASK 0x00000002
/**
 * @brief Bit mask to select or set output 3 for simulation.
 */
#define ROMULUS_DATA_SIM_MATRIX_OUTPUT_3_MASK 0x00000004
/**
 * @brief Bit mask to select or set output 4 for simulation.
 */
#define ROMULUS_DATA_SIM_MATRIX_OUTPUT_4_MASK 0x00000008
/**
 * @brief Bit mask to select or set auxiliary output 1 for simulation.
 */
#define ROMULUS_DATA_SIM_MATRIX_OUTPUT_AUX_1_MASK 0x00000010
/**
 * @brief Bit mask to select or set auxiliary output 2 for simulation.
 */
#define ROMULUS_DATA_SIM_MATRIX_OUTPUT_AUX_2_MASK 0x00000020
/**
 * @brief Bit mask to select or set auxiliary output 3 for simulation.
 */
#define ROMULUS_DATA_SIM_MATRIX_OUTPUT_AUX_3_MASK 0x00000040

/**
 * @brief Bit mask to select or set the persistant fault for simulation.
 */
#define ROMULUS_DATA_SIM_MATRIX_INPUT_PERSISTANT_FAULT_MASK 0x00000001
/**
 * @brief Bit mask to select or set the major fault for simulation.
 */
#define ROMULUS_DATA_SIM_MATRIX_INPUT_MAJOR_FAULT_MASK 0x00000002
/**
 * @brief Bit mask to select or set the minor fault for simulation.
 */
#define ROMULUS_DATA_SIM_MATRIX_INPUT_MINOR_FAULT_MASK 0x00000004
/**
 * @brief Bit mask to select or set the integral alarm 2 for simulation.
 */
#define ROMULUS_DATA_SIM_MATRIX_INPUT_INTEGRAL_ALARM_2_MASK 0x00000008
/**
 * @brief Bit mask to select or set the integral alarm 1 for simulation.
 */
#define ROMULUS_DATA_SIM_MATRIX_INPUT_INTEGRAL_ALARM_1_MASK 0x00000010
/**
 * @brief Bit mask to select or set the special alarm for simulation.
 */
#define ROMULUS_DATA_SIM_MATRIX_INPUT_SPECIAL_ALARM_MASK 0x00000020
/**
 * @brief Bit mask to select or set the alarm for simulation.
 */
#define ROMULUS_DATA_SIM_MATRIX_INPUT_ALARM_MASK 0x00000040
/**
 * @brief Bit mask to select or set the alert for simulation.
 */
#define ROMULUS_DATA_SIM_MATRIX_INPUT_ALERT_MASK 0x00000080
/**
 * @brief Bit mask to select or set the input 4 for simulation.
 */
#define ROMULUS_DATA_SIM_MATRIX_INPUT_4_MASK 0x00000100
/**
 * @brief Bit mask to select or set the input 3 for simulation.
 */
#define ROMULUS_DATA_SIM_MATRIX_INPUT_3_MASK 0x00000200
/**
 * @brief Bit mask to select or set the input 2 for simulation.
 */
#define ROMULUS_DATA_SIM_MATRIX_INPUT_2_MASK 0x00000400
/**
 * @brief Bit mask to select or set the input 1 for simulation.
 */
#define ROMULUS_DATA_SIM_MATRIX_INPUT_1_MASK 0x00000800

/**
 * @brief Value to flag base values in historic data.
 */
#define ROMULUS_DATA_MINUTE_HOUR_RAW 0
/**
 * @brief Value to flag minute values in historic data.
 */
#define ROMULUS_DATA_MINUTE_HOUR_MINUTE 1
/**
 * @brief Value to flag hour values in historic data.
 */
#define ROMULUS_DATA_MINUTE_HOUR_HOUR 2

#include "_romulus_status_struct.h"

#include "_romulus_rtmeas_struct.h"

#include "_romulus_hist_data_struct.h"

#include "_romulus_hist_ts_struct.h"

#include "_romulus_hist_event_struct.h"
/** @} */

/** @defgroup romulus_initialisation Functions to set up ROMULUSlib
 * Function to initialise the ROMULUSlib.
 * @{
 */

/**
 * @brief romulus_init Initialises the library. Has to be called prior to any use of any other library function.
 * @param local_id Local ID of the device, supervision or whatever else.
 * @return Returns 0 on success and -1 on error.
 */
int romulus_init(const char* const local_id);
/** @} */

/** @defgroup romulus_debug Functions for manipulating the amount of log
 * All function and type definitions to query and manipulate the logging.
 * @{
 */

/**
  * @brief Wrapper struct type to emulate an enum which sets the debug level.
  */
typedef struct{
    int level; /**< Integer which is signifies increasing verbosity the higher it gets. */
} romulus_log_level_t;

/**
 * @brief Unwrapped value for default verbosit ROMULUS_DEBUG_LEVEL_INFO
 */
#define _ROMULUS_LOG_LEVEL_INFO   2 // TODO: remove as soon as GCC >= 4.9

/**
 * @brief Lowest verbosity, only errors are shown. The log level cannot be lower.
 */
#define ROMULUS_LOG_LEVEL_ERROR   ((const romulus_log_level_t){0})
/**
 * @brief Second lowest verbosity, only errors and warnings are shown.
 */
#define ROMULUS_LOG_LEVEL_WARNING ((const romulus_log_level_t){1})
/**
 * @brief Default level of verbosity. This level is recommended to be used in production systems.
 */
#define ROMULUS_LOG_LEVEL_INFO    ((const romulus_log_level_t){_ROMULUS_LOG_LEVEL_INFO}) // TODO: remove _ROMULUS_LOG_LEVEL_INFO as soon as GCC >= 4.9
/**
 * @brief Highest level of verbosity. This level should only be used for debugging.
 */
#define ROMULUS_LOG_LEVEL_DEBUG   ((const romulus_log_level_t){3})

/**
 * @brief romulus_debuglevel_get Read the romulus debug level of the calling thread.
 * @return Returns the current romulus debug level of the current thread. This function always succeeds.
 */
romulus_log_level_t romulus_log_level_get(void);
/**
 * @brief romulus_log_level_set Sets the romulus debug level of the calling thread.
 * @param new_log_level The new romulus debug level.
 */
void romulus_log_level_set(romulus_log_level_t new_log_level);
/** @} */

/** @defgroup romulus_time Time functions definitions
 * Function definitions to generate timestamp in machine and human readable form.
 * The timestamps are created by counting miliseconds since UNIX epoch.
 * @{
 */

/**
 * @brief romulus_timestamp_now Returns the number of miliseconds since UNIX epoch.
 * @return Returns the time now on success or -1 on error.
 */
int64_t romulus_timestamp_now();
/**
 * @brief romulus_timestamp_str Converts a romulus timestamp to a string.
 * @param timestamp Timestamp to be converted.
 * @param buff Buffer where the converted timestamp has to be stored. Must hold at least 25 bytes.
 * @return Return NULL on error or a pointer to the buff.
 */
char *romulus_timestamp_str(int64_t timestamp, char* buff);
/** @} */

/** @defgroup romulus_version Version functions definitions
 * Function definitions to read the version of the library.
 * @{
 */

/**
 * @brief romulus_get_version_str Returns the version of the romulus library in form of a immutable string.
 * @return Pointer to the read-only string of the version of the library.
 */
const char* romulus_get_version_str(void);
/**
 * @brief romulus_get_commit_id Returns the commit ID of when the library was compiled.
 * @return Pointer to the read-only string of the commit ID of the library.
 */
const char* romulus_get_commit_id(void);

/** @} */

/** @defgroup romulus_data_types Data type definitions
 * Function and data type definition for ROMULUSlib's internal types
 * @{
 */

/**
 * @brief Struct type to encapsulate type codes for romulus parameters.
 */
typedef struct{
    int type; /**< Actual data which encodes the type of the parameter */
} romulus_type_info_type_wrapper_t;

/**
 * @brief Unwrapped value which encodes the type of a paramter, which is not defined.
 * No paramter can have this type, e.g. it can be used when a type is not set.
 */
#define _ROMULUS_DATA_TYPE_undef          0
/**
 * @brief Unwrapped value which encodes the type of a paramter, which is of boolean type.
 */
#define _ROMULUS_DATA_TYPE_bool           1
/**
 * @brief Unwrapped value which encodes the type of a paramter, which is a byte.
 */
#define _ROMULUS_DATA_TYPE_uint8_t        2
/**
 * @brief Unwrapped value which encodes the type of a paramter, which is a 4 byte integer.
 */
#define _ROMULUS_DATA_TYPE_int32_t        3
/**
 * @brief Unwrapped value which encodes the type of a paramter, which is 8 byte integer.
 */
#define _ROMULUS_DATA_TYPE_int64_t        4
/**
 * @brief Unwrapped value which encodes the type of a paramter, which is a float.
 */
#define _ROMULUS_DATA_TYPE_float          5
/**
 * @brief Unwrapped value which encodes the type of a paramter, which is an array of 16 indivitual bytes.
 * These bytes are interpreted as characters and the array does not need to be null/zero terminated.
 */
#define _ROMULUS_DATA_TYPE_uint8_t_16     6
/**
 * @brief Unwrapped value which encodes the type of a paramter, which is an array of 64 indivitual bytes.
 * These bytes are interpreted as characters and the array does not need to be null/zero terminated.
 */
#define _ROMULUS_DATA_TYPE_uint8_t_64     7
/**
 * @brief Unwrapped value which encodes the type of a paramter, which is an array of an array of booleans.
 * The dimensions of the array 11 for the array of arrays and 40 for the array. Within an array, two
 * consequetive booleans have to be read together to form the trooth table of [00 = 0; 01 = 1; 10 = -1; 11 = error].
 */
#define _ROMULUS_DATA_TYPE_bool_11_40     8

/**
 * @brief Alias of _ROMULUS_DATA_TYPE_bool
 */
#define _ROMULUS_DATA_TYPE__Bool         _ROMULUS_DATA_TYPE_bool
/**
 * @brief Alias of _ROMULUS_DATA_TYPE_bool_11_40
 */
#define _ROMULUS_DATA_TYPE__Bool_11_40   _ROMULUS_DATA_TYPE_bool_11_40

/**
 * @brief Typesafe version of _ROMULUS_DATA_TYPE_undef
 */
#define ROMULUS_DATA_TYPE_undef         ((const romulus_type_info_type_wrapper_t){_ROMULUS_DATA_TYPE_undef})
/**
 * @brief Typesafe version of _ROMULUS_DATA_TYPE_bool
 */
#define ROMULUS_DATA_TYPE_bool          ((const romulus_type_info_type_wrapper_t){_ROMULUS_DATA_TYPE_bool})
/**
 * @brief Typesafe version of _ROMULUS_DATA_TYPE_uint8_t
 */
#define ROMULUS_DATA_TYPE_uint8_t       ((const romulus_type_info_type_wrapper_t){_ROMULUS_DATA_TYPE_uint8_t})
/**
 * @brief Typesafe version of _ROMULUS_DATA_TYPE_int32_t
 */
#define ROMULUS_DATA_TYPE_int32_t       ((const romulus_type_info_type_wrapper_t){_ROMULUS_DATA_TYPE_int32_t})
/**
 * @brief Typesafe version of _ROMULUS_DATA_TYPE_int64_t
 */
#define ROMULUS_DATA_TYPE_int64_t       ((const romulus_type_info_type_wrapper_t){_ROMULUS_DATA_TYPE_int64_t})
/**
 * @brief Typesafe version of _ROMULUS_DATA_TYPE_float
 */
#define ROMULUS_DATA_TYPE_float         ((const romulus_type_info_type_wrapper_t){_ROMULUS_DATA_TYPE_float})
/**
 * @brief Typesafe version of _ROMULUS_DATA_TYPE_uint8_t_16
 */
#define ROMULUS_DATA_TYPE_uint8_t_16    ((const romulus_type_info_type_wrapper_t){_ROMULUS_DATA_TYPE_uint8_t_16})
/**
 * @brief Typesafe version of _ROMULUS_DATA_TYPE_uint8_t_64
 */
#define ROMULUS_DATA_TYPE_uint8_t_64    ((const romulus_type_info_type_wrapper_t){_ROMULUS_DATA_TYPE_uint8_t_64})
/**
 * @brief Typesafe version of _ROMULUS_DATA_TYPE_bool_11_40
 */
#define ROMULUS_DATA_TYPE_bool_11_40    ((const romulus_type_info_type_wrapper_t){_ROMULUS_DATA_TYPE_bool_11_40})

/**
 * @brief Alias of ROMULUS_DATA_TYPE_bool
 */
#define ROMULUS_DATA_TYPE__Bool         ROMULUS_DATA_TYPE_bool
/**
 * @brief Alias of ROMULUS_DATA_TYPE_bool_11_40
 */
#define ROMULUS_DATA_TYPE__Bool_11_40   ROMULUS_DATA_TYPE_bool_11_40

/**
 * @brief Struct which contains information about the layout, content, etc of romulus paramters.
 */
typedef struct{
    char* name; /**< The string of the name of the romulus struct member. */
    romulus_type_info_type_wrapper_t type; /**< Information which data type the romulus struct member is. */
    ssize_t size_unmarshalled; /**< The unmarshalled size of the romulus struct member. */
    ssize_t size_marshalled; /**< The marshalled size of the romulus struct member. */
    ssize_t offset; /**< The offset of the romulus struct member within the struct. */
    uint8_t id; /**< The ID code which identifies the romulus struct member. */
} romulus_type_info_t;

/**
 * @brief _romulus_param_type_info Array which is used by functions to get romulus paramter type information.
 * This struct should never be accessed directly. Instead, use functions such as ROMULUS_TYPE_INFO_BY_NAME,
 * ROMULUS_TYPE_INFO_BY_ID, ROMULUS_TYPE_REFERENCE_TO_MEMBER_BY_NAME, romulus_type_reference_to_member_by_id
 * or _romulus_type_reference_to_member_by_id to access the struct.
 */
extern const romulus_type_info_t _romulus_params_type_info[sizeof(romulus_params_t)];
/**
 * @brief _romulus_param_type_info_by_id Array which is used to look up romulus status type information by the romulus type ID.
 * Never use this function directly, instead use romulus_type_reference_to_member_by_id or
 * _romulus_type_reference_to_member_by_id.
 */
extern const romulus_type_info_t* _romulus_params_type_info_by_id[(1 << 8) * sizeof(uint8_t)];
/**
 * @brief ROMULUS_PARAMETERS_COUNT The number of romulus paramters. All the romulus paramter IDs are smaller than ROMULUS_PARAMETERS_COUNT.
 */
extern const unsigned int ROMULUS_PARAMETERS_COUNT;

/**
 * @brief _romulus_status_type_info Look-up table to get type information for the status struct.
 * This struct should never be accessed directly. Instead, use functions such as ROMULUS_TYPE_INFO_BY_NAME,
 * ROMULUS_TYPE_INFO_BY_ID, ROMULUS_TYPE_REFERENCE_TO_MEMBER_BY_NAME, romulus_type_reference_to_member_by_id
 * or _romulus_type_reference_to_member_by_id to access the struct.
 */
extern const romulus_type_info_t _romulus_status_type_info[sizeof(romulus_status_t)];
/**
 * @brief _romulus_status_type_info_by_id Array which is used to look up romulus status type information by the romulus type ID.
 * Never use this function directly, instead use romulus_type_reference_to_member_by_id or
 * _romulus_type_reference_to_member_by_id.
 */
extern const romulus_type_info_t* _romulus_status_type_info_by_id[(1 << 8) * sizeof(uint8_t)];
/**
 * @brief ROMULUS_STATUS_COUNT The number of romulus stati. All the romulus status IDs are smaller than ROMULUS_STATUS_COUNT.
 */
extern const unsigned int ROMULUS_STATUS_COUNT;

/**
 * @brief Convenience macro to construct tokens based on a prefix + core + suffix.
 * @param _prefix The prefix of the token.
 * @param _name The core of the token.
 * @param _suffix The suffix of the token.
 * @return Returns a token composed of the coercion of prefix + core + suffix.
 */
#define _CREATE_NAME(_prefix, _name, _suffix)  _prefix ## _ ## _name ## _ ## _suffix
/**
 * @brief Unsafe version of ROMULUS_TYPE_INFO_BY_NAME. If you know what you are doing and are not afraid of API changes which might mess up your system, you can use this function.
 * A legit use case where the safe version does not work is when using it as a function paramter or within an expression as the static asserts to make it safe cannot go there.
 * Alternatively to use the unsafe version, you can also use a temporary variable like const romulus_type_info_t* tmp = ROMULUS_TYPE_INFO_BY_NAME(...) and the do foo(tmp), if(tmp->id == SOME_ID) or similiar.
 * @param _stipped_struct_name Stripped down name of the desired struct: romulus_params_t -> params; romulus_local_params_t -> local_params; romulus_status_t -> status.
 * @param _field_name Name of the romulus paramter struct's field in plain text (not a string).
 * @return Always succeeds at runtime (would fail at compile time) and returns the romulus paramter information struct for that parameter.
 */
#define _ROMULUS_TYPE_INFO_BY_NAME(_stipped_struct_name, _field_name)\
    (_CREATE_NAME(_romulus, _stipped_struct_name, type_info) + offsetof(_CREATE_NAME(romulus, _stipped_struct_name, t), _field_name))
/**
 * @brief Gives the romulus paramter information by looking it up through its name.
 * @param _stipped_struct_name Stripped down name of the desired struct: romulus_params_t -> params; romulus_local_params_t -> local_params; romulus_status_t -> status.
 * @param _field_name Name of the romulus paramter struct's field in plain text (not a string).
 * @return Always succeeds at runtime (would fail at compile time) and returns the romulus paramter information struct for that parameter.
 */
#define ROMULUS_TYPE_INFO_BY_NAME(_stipped_struct_name, _field_name)\
    _ROMULUS_TYPE_INFO_BY_NAME(_stipped_struct_name, _field_name);\
    static_assert(sizeof(_CREATE_NAME(_romulus, _stipped_struct_name, type_info)) == sizeof(_romulus_params_type_info) ||\
    sizeof(_CREATE_NAME(_romulus, _stipped_struct_name, type_info)) == sizeof(_romulus_status_type_info),\
    "Cannot access this type info array. Only _romulus_status_type_info and _romulus_params_type_info (and _romulus_local_params_type_info, but not with this macro) may be accessed safely.")
/**
 * @brief romulus_type_info_by_id Gives the romulus paramter information by looking it up thourgh its ID.
 * @param type_info_by_id_lut Look-up table to be used to get type info. It can be either the one for romulus paramters, local parameters or status, e.g. either _romulus_params_type_info_by_id, _romulus_local_params_type_info_by_id or _romulus_status_type_info_by_id.
 * @param id The ID of the romulus paramters.
 * @return Returns NULL on error (e.g. when the ID is bigger than ROMULUS_PARAMETERS_COUNT or ROMULUS_LOCAL_PARAMETERS_COUNT) or a pointer to the romulus (local) paramter information struct.
 */
const romulus_type_info_t* romulus_type_info_by_id(const romulus_type_info_t* const* const type_info_by_id_lut, const uint8_t id);

/**
 * @brief Gives a const void pointer to the base address of the field in the romulus paramters struct.
 * @param _params_ptr Pointer to the romulus paramters struct.
 * @param _stipped_struct_name Stripped down name of the desired struct: romulus_params_t -> params; romulus_local_params_t -> local_params.
 * @param _field_name Name of the romulus paramter struct's field in plain text (not a string).
 * @return Always suceeds at runtime (can fail on compile time) and returns a const void pointer to the base address of the requested
 * field in the romulus paramters struct.
 */
#define ROMULUS_PARAM_CONST_REFERENCE_TO_MEMBER_BY_NAME(_params_ptr, _stipped_struct_name, _field_name)\
    ((const void* const)(((uint8_t*)_params_ptr) + ROMULUS_TYPE_INFO_BY_NAME(_stipped_struct_name, _field_name)->offset));\
    static_assert(sizeof(*_params_ptr) == sizeof(romulus_params_t) || sizeof(*_params_ptr) == sizeof(romulus_local_params_t), "Bad type for function-like macro. Make sure to use a pointer to the struct")
/**
 * @brief Gives a non-const void pointer to the base address of the field in the romulus paramters struct.
 * @param _params_ptr Pointer to the romulus paramters struct.
 * @param _stipped_struct_name Stripped down name of the desired struct: romulus_params_t -> params; romulus_local_params_t -> local_params.
 * @param _field_name Name of the romulus paramter struct's field in plain text (not a string).
 * @return Always suceeds at runtime (can fail on compile time) and returns a void pointer to the base address of the requested
 * field in the romulus paramters struct.
 */
#define _ROMULUS_PARAM_REFERENCE_TO_MEMBER_BY_NAME(_params_ptr, _stipped_struct_name, _field_name)\
    ((void* const)(((uint8_t*)_params_ptr) + ROMULUS_TYPE_INFO_BY_NAME(_stipped_struct_name, _field_name)->offset));\
    static_assert(sizeof(*_params_ptr) == sizeof(romulus_params_t) || sizeof(*_params_ptr) == sizeof(romulus_local_params_t), "Bad type for function-like macro. Make sure to use a pointer to the struct")
/**
 * @brief romulus_param_const_reference_to_member_by_id Gives a const void pointer to the field defined by its ID within a romulus paramters struct.
 * @param params Pointer to the paramters struct (romulus_params_t or romulus_local_params_t) of which a field has to be accessed.
 * @param type_info_by_id_lut Pointer to the look-up table from which the type info by ID as to be taken (either _romulus_params_type_info_by_id or _romulus_local_params_type_info_by_id, depending on the type of params).
 * @param id ID if the field which has to be accessed.
 * @return Returns NULL on error or a const reference to the base address of the requested field within the romulus paramters struct.
 */
const void* romulus_param_const_reference_to_member_by_id(const void* const params, const romulus_type_info_t* const* const type_info_by_id_lut, const uint8_t id);
/**
 * @brief romulus_param_const_reference_to_member_by_id Gives a non-const void pointer to the field defined by its ID within a romulus paramters struct.
 * @param params Pointer to the paramters struct (romulus_params_t or romulus_local_params_t) of which a field has to be accessed.
 * @param type_info_by_id_lut Pointer to the look-up table from which the type info by ID as to be taken (either _romulus_params_type_info_by_id or _romulus_local_params_type_info_by_id, depending on the type of params).
 * @param id ID if the field which has to be accessed.
 * @return Returns NULL on error or a modifieable reference to the base address of the requested field within the romulus paramters struct.
 */
void* _romulus_param_reference_to_member_by_id(void* const params, const romulus_type_info_t* const* const type_info_by_id_lut, const uint8_t id);
/** @} */

/** @defgroup romulus_param_protection Definitions to create paramter protection masks
 * Definitions to create paramter protection masks
 * @{
 */

/**
 * @brief Wrapper struct which emulates an enum to protect romulus parameters from being written thourgh a frame.
 */
typedef struct{
    int protection; /**< Actual value to check if the romulus paramter is protected. */
} romulus_param_protection_t;

/**
 * @brief Paramter is both read and writable.
 */
#define ROMULUS_PARAMTER_PROTECTION_READ_WRITE  ((romulus_param_protection_t){0})
/**
 * @brief Paramter is write protected and can only be read.
 */
#define ROMULUS_PARAMTER_PROTECTION_READ_ONLY   ((romulus_param_protection_t){-1})
/** @} */

#endif /* ROMULUS_H_ */
