
/* Copyright (C) 1991-2016 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */




/* This header is separate from features.h so that the compiler can
   include it implicitly at the start of every compilation.  It must
   not itself include <features.h> or any other header that includes
   <features.h> because the implicit include comes before any feature
   test macros that may be defined in a source file before it first
   explicitly includes a system header.  GCC knows the name of this
   header in order to preinclude it.  */

/* glibc's intent is to support the IEC 559 math functionality, real
   and complex.  If the GCC (4.9 and later) predefined macros
   specifying compiler intent are available, use them to determine
   whether the overall intent is to support these features; otherwise,
   presume an older compiler has intent to support these features and
   define these macros by default.  */
/* wchar_t uses Unicode 8.0.0.  Version 8.0 of the Unicode Standard is
   synchronized with ISO/IEC 10646:2014, plus Amendment 1 (published
   2015-05-15).  */


/* We do not support C11 <threads.h>.  */

/** @brief to be done @ingroup romulus_structs */ 
typedef struct _romulus_packed {

int64_t TimeStamp; /**< Timestamp of the specific sample measured in miliseconds since POSIX epoch. */

float Temperature; /**< Same as romulus_rtmeas_t#Temperature. */
float Humidity; /**< Same as romulus_rtmeas_t#Humidity. */

int32_t CAUStatus; /**< Status of the CAU. The CAU status is decoded as follows: bit 1 downto 0 form the Alarm Mode Status, bit 2 indicates a System Fault, bit 3 indicates Alert Weak, bit 4 indicates Alert Fault, bit 5 indicates Power Fault, bit 6 indicates Transmission Fault, bit 7 indicates Battery Fault and bit 8 inidcates AC Fault.  */
int32_t TimeSync; /**< Time in seconds since the last time synchronisation of the system. */

bool Alert; /**< Defines if there was an alert. */
bool Alarm; /**< Defines if there was an alarm. */
bool SpecialAlarm; /**< Defines if there was a special alarm. */
bool MinorFault; /**< Defines if there was a minor fault. */
bool MajorFault; /**< Defines if there was a major fault. */
bool PersistentFault; /**< Defines if there was a persistant fault. */
bool TimeSyncFault; /**< Defines if there was an error while trying to synchronise the time. */
bool MinValueFault; /**< Defines if there was a Minimum Value fault. See romulus_rtmeas_t#MinValue for more information. */
bool MaxValueFault; /**< Defines if there was a Maximum Value fault. See romulus_rtmeas_t#MaxValue for more information. */
bool TempFault; /**< Defines if there was a temperature fault. A temperature fault can be trigger when going above or below two thresholds and to clear the fault the temperature has to go below a certain hysteresis value. Thresholds and hysteresis values are set locally per device. */
bool TempHighFault; /**< Defines if there is a high temperature fault. This fault is triggered when the temperature goes above a certain treshold and is clear after it has gone below a certain hysteresis value. Thresholds and hysteresis values are set locally per device. */
bool TempDamageFault; /**< Defines if there is a temperature fault which can damage the device. This fault is triggered when the temperature goes above a certain treshold and is clear after it has gone below a certain hysteresis value. Thresholds and hysteresis values are set locally per device. */
bool HumidityFault; /**< Defines if there is a humidity fault. A humidity fault can be trigger when going above or below two thresholds and to clear the fault the humidity has to go below a certain hysteresis value. Thresholds and hysteresis values are set locally per device. */
bool PowerFault; /**< Defines if there is a power fault. */
bool BatteryFault; /**< Defines if there is a batter fault. */
bool ChargerFault; /**< Defines if there is a chager fault. */
bool InputCurrentFault; /**< Defines if the input current was too high (which can damage the device) to cause an input current fault. The value of this current and its hysteresis is locally settable. */
bool HVnominalFault; /**< Defines if there is a high voltage nominal fault, e.g. when the set high voltage differs by more than 1% or the measured high voltage. */
bool TempCoherenceFault; /**< Defines if there is a temperature coherency fault, e.g. if the measured temperatures differ by more than 5°C. */
bool HVFault; /**< Defines if there is a high voltage fault. */
bool LVFault; /**< Defines if there is a low voltage fault. */
bool FPGAFault; /**< Defines if there is an FPGA fault. */
bool CAULinkFault; /**< Defines if there is a CAU link fault. */
bool IntegralAlarm1; /**< Defines if romulus_rtmeas_t#Intergral1 exceeded romulus_params_t#Integral1Tresh and caused a Integral1 fault and has not yet been reset with romulus_params_t#Integral1AlarmReset. */
bool IntegralAlarm2; /**< Defines if romulus_rtmeas_t#Intergral2 exceeded romulus_params_t#Integral2Tresh and caused a Integral2 fault and has not yet been reset with romulus_params_t#Integral2AlarmReset. */

bool Input1; /**< Input1 state. */
bool Input2; /**< Input2 state. */
bool Input3; /**< Input3 state. */
bool Input4; /**< Input4 state. */
bool Output1; /**< Output1 state. */
bool Output2; /**< Output2 state. */
bool Output3; /**< Output3 state. */
bool Output4; /**< Output4 state. */

bool NetworkConnection; /**< Defines if there is a network connection fault. */
bool SensorConnection; /**< Defines if there is a sensor connection fault. */

bool HistDataStorageFailure; /**< Defines if there is a historic data storage failure. */
bool FreeRAMWarning; /**< Defines if the amount of free RAM for the system goes below a certain locally defined threshold. */
bool FreeStorageWarning; /**< Defines if the amount of free storage for the system goes below a certain locally defined threshold. */

uint8_t CAUAMCOS; /**< Value of the CAUAMCOS sent back by the CAU: 0x00 means system fault, 0x01 means alarm, 0x02 means alert, 0x03 means system ok. */
uint8_t Mode; /**< Same as romulus_params_t#Mode. */

} romulus_status_t;
