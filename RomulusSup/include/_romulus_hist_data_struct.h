
/* Copyright (C) 1991-2016 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */




/* This header is separate from features.h so that the compiler can
   include it implicitly at the start of every compilation.  It must
   not itself include <features.h> or any other header that includes
   <features.h> because the implicit include comes before any feature
   test macros that may be defined in a source file before it first
   explicitly includes a system header.  GCC knows the name of this
   header in order to preinclude it.  */

/* glibc's intent is to support the IEC 559 math functionality, real
   and complex.  If the GCC (4.9 and later) predefined macros
   specifying compiler intent are available, use them to determine
   whether the overall intent is to support these features; otherwise,
   presume an older compiler has intent to support these features and
   define these macros by default.  */
/* wchar_t uses Unicode 8.0.0.  Version 8.0 of the Unicode Standard is
   synchronized with ISO/IEC 10646:2014, plus Amendment 1 (published
   2015-05-15).  */


/* We do not support C11 <threads.h>.  */

/** @brief Data type to store historic data. @ingroup romulus_structs */ 
typedef struct _romulus_packed {

int64_t TimeStamp; /**< Timestamp of the specific sample measured in miliseconds since POSIX epoch. */
int32_t MSID; /**< Same as romulus_params_t#MSID. */
float AvgValue; /**< Same as romulus_rtmeas_t#AvgValue. */
float Integral1; /**< Same as romulus_rtmeas_t#Integral1. */
float Integral2; /**< Same as romulus_rtmeas_t#Integral2. */

float MinValue; /**< Same as romulus_rtmeas_t#MinValue. */
float MaxValue; /**< Same as romulus_rtmeas_t#MaxValue. */
float HighVoltage; /**< High voltage measured at the ionisation chamber. */
float Temperature; /**< Same as romulus_rtmeas_t#Temperature. */
float Humidity; /**< Same as romulus_rtmeas_t#Humidity. */

float Value; /**< Measured radiation level. This can be the base-, minute- or hour-value, depending on the value of romulus_hist_data_t#MinutHourFlag and set in units as specified in romulus_params_t#MeasUnit. */
uint8_t MinutHourFlag; /**< Flag which indicates the interpretation of Value, Alert, Alarm, SpecialAlarm, MinorFault, MajorFault and PersistentFault. When 0x00, the values are based on a measurment cycle, when 0x01, the values are averaged over a minute, when 0x02, the values are averaged over an hour. */

uint8_t Mode; /**< Same as romulus_params_t#Mode. */

uint8_t MeasUnit; /**< Same as romulus_params_t#MeasUnit. */
uint8_t IntegralAlarms; /**< Defines if one of the integrals has an alarm. Bit 0 is for integral 1 and bit 1 is for integral 2. If bit is set, then there is an alarm. */

bool Alert; /**< Defines if there was an alert within the time frame specified by romulus_hist_data_t#MinutHourFlag. The field has always a kind of a latching behaviour during one time frame: If it was high at some point, the value will be high and only be set to low in the next time frame after the value has gone low. */
bool Alarm; /**< Defines if there was an alarm within the time frame specified by romulus_hist_data_t#MinutHourFlag.  The field has always a kind of a latching behaviour during one time frame: If it was high at some point, the value will be high and only be set to low in the next time frame after the value has gone low. */
bool SpecialAlarm; /**< Defines if there was a special alarm within the time frame specified by romulus_hist_data_t#MinutHourFlag. The field has always a kind of a latching behaviour during one time frame: If it was high at some point, the value will be high and only be set to low in the next time frame after the value has gone low. */
bool MinorFault; /**< Defines if there was an minor fault within the time frame specified by romulus_hist_data_t#MinutHourFlag. The field has always a kind of a latching behaviour during one time frame: If it was high at some point, the value will be high and only be set to low in the next time frame after the value has gone low. */
bool MajorFault; /**< Defines if there was an major fault within the time frame specified by romulus_hist_data_t#MinutHourFlag. The field has always a kind of a latching behaviour during one time frame: If it was high at some point, the value will be high and only be set to low in the next time frame after the value has gone low. */
bool PersistentFault; /**< Defines if there was an persistant fault within the time frame specified by romulus_hist_data_t#MinutHourFlag. The field has always a kind of a latching behaviour during one time frame: If it was high at some point, the value will be high and only be set to low in the next time frame after the value has gone low. */

} romulus_hist_data_t;
