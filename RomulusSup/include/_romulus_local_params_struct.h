
/* Copyright (C) 1991-2016 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */




/* This header is separate from features.h so that the compiler can
   include it implicitly at the start of every compilation.  It must
   not itself include <features.h> or any other header that includes
   <features.h> because the implicit include comes before any feature
   test macros that may be defined in a source file before it first
   explicitly includes a system header.  GCC knows the name of this
   header in order to preinclude it.  */

/* glibc's intent is to support the IEC 559 math functionality, real
   and complex.  If the GCC (4.9 and later) predefined macros
   specifying compiler intent are available, use them to determine
   whether the overall intent is to support these features; otherwise,
   presume an older compiler has intent to support these features and
   define these macros by default.  */
/* wchar_t uses Unicode 8.0.0.  Version 8.0 of the Unicode Standard is
   synchronized with ISO/IEC 10646:2014, plus Amendment 1 (published
   2015-05-15).  */


/* We do not support C11 <threads.h>.  */

/** @brief Local paramters struct, which is used to parametrise locally the system. @ingroup romulus_structs */ 
typedef struct _romulus_packed {

uint8_t Serial[16]; /**< Serial of the monitor. */
uint8_t FuncPosition[16]; /**< Functional position of the monitor. */

int32_t RomulusPort; /**< Main port used for all the ROMULUS communication. */

int32_t PLTimeUpdateFrequencyInMiliseconds; /**< Frequency in miliseconds with which the PL time is updated. */

float CInt; /**< Capacity of the frontend capacity to measure currents in farads (usually 100e-12 F). */

float AddLog2BitsToPL; /**< How many bits are being added to some values by shifting them in the PL. */

float HighVoltageGain; /**< Gain of the high voltage module (in percent (0.0 to 1.0) and calculated as 1.0 + romulus_local_params_t#HighVoltageGain to get the actual value). */

float HighVoltageNominalPercentage; /**< Threshold percentage (0.0 to 1.0) outside which the high voltage triggers a persistant fault. Has an implicit hysteresis value with romulus_local_params_t#HighVoltageFaultHysteresisPercentage. Must be bigger than romulus_local_params_t#HighVoltageFaultPercentage. */
float HighVoltageFaultPercentage; /**< Threshold percentage (0.0 to 1.0 outside which the high voltage trigger a minor or major fault. This value must be smaller or equal than romulus_local_params_t#HighVoltageNominalPercentage. */
float HighVoltageFaultHysteresisPercentage; /**< Hysteresis percentage (0.0 to 1.0) for high voltage faults. Is used for both romulus_local_params_t#HighVoltageNominalPercentage and romulus_local_params_t#HighVoltageFaultPercentage. Is calcualted like 0.1 * (1.0 - 0.1) = 0.09. */

float LowVoltageFaultPercentage; /**< Percentage by how much the low voltages can vary from their set value before triggering a fault (0.0 to 1.0). */
float LowVoltageFaultHysteresisPercentage; /**< Percentage for the hysteresis value, below (or above) which the low voltage has to drop with regard to romulus_local_params_t#LowVoltageFaultPercentage (0.0 to 1.0, example: 0.1 * (1.0 - 0.1) = 0.09). */

float CurrentFaultThreshold; /**< Threshold for triggering an input current fault (in amperes and usually 4e-6 A). */
float CurrentFaultThresholdHysteresis; /**< Histeresis value for the input current fault (in amperes, must be lower or equal than romulus_local_params_t#CurrentFaultThreshold and usually 3.8e-6 A) */

float RelativeHumidityFaultUp; /**< Threshold for the upper bound to trigger a humidity fault (in percentage (0.0 to 1.0)). */
float RelativeHumidityFaultUpHysteresis; /**< Hysteresis value for the upper bound to trigger a humidity fault (in percentage (0.0 to 1.0) and must be lower or equal to romulus_local_params_t#RelativeHumidityFaultUp). */
float RelativeHumidityFaultLow; /**< Threshold for the lower bound to trigger a humidity fault (in percentage (0.0 to 1.0)). */
float RelativeHumidityFaultLowHysteresis; /**< Hysteresis value for the lower bound to trigger a humidity fault (in percentage (0.0 to 1.0) and must be higher or equal to romulus_local_params_t#RelativeHumidityFaultLow). */

int32_t TemperatureFaultUp; /**< Thresholds for the upper bound the the specified operating temperature range (in °C and entire numbers). */
int32_t TemperatureFaultUpHysteresis; /**< Hysteresis value for the upper bound the the specified operating temperature range (in °C, entire numbers and must be lower or equal than romulus_local_params_t#TemperatureFaultUp). */
int32_t TemperatureFaultLow; /**< Thresholds for the lower bound the the specified operating temperature range (in °C and entire numbers). */
int32_t TemperatureFaultLowHysteresis; /**< Hysteresis value for the lower bound the the specified operating temperature range (in °C, entire numbers and must be higher or equal than romulus_local_params_t#TemperatureFaultLow). */
int32_t TemperatureFaultHigh; /**< Comments to be done */
int32_t TemperatureFaultHighHysteresis; /**< Comments to be done */
int32_t TemperatureFaultDamage; /**< Threshold for damaging high voltage faults (in °C and entire numbers). */
int32_t TemperatureFaultDamageHysteresis; /**< Histeresis value for damaging high voltage faults (in °C, entire numbers and must be higher or equal than romulus_local_params_t#TemperatureFaultDamage). */

float MinMaxFaultHysteresis; /**< Histeresis percentage for the min and max value fault (between 0.0 and 1.0). */

int64_t cmpuFaultCategory; /**< Fault categories for the CMPU; check the documentation of the faults core for more information. */
int64_t cauFaultCategory; /**< Fault categories for the CAU; check the documentation of the faults core for more information. */

float TemperatureCoefficient0; /**< Coefficient of the 0 order of the polynomial for temperature compensation. */
float TemperatureCoefficient1; /**< Coefficient of the 1 order of the polynomial for temperature compensation. */
float TemperatureCoefficient2; /**< Coefficient of the 2 order of the polynomial for temperature compensation. */
float TemperatureCoefficient3; /**< Coefficient of the 3 order of the polynomial for temperature compensation. */
float TemperatureCoefficient4; /**< Coefficient of the 4 order of the polynomial for temperature compensation. */

int32_t HistRetentionTime; /**< Minimum time for which historic data and events are being retained (in miliseconds). */
int32_t HistDataFileSize; /**< Maximum file size for historic data (in bytes). */
int32_t HistEventsFileSize; /**< Maximum file size for historic events (in bytes). */

} romulus_local_params_t;
