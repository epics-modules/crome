
/* Copyright (C) 1991-2016 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */




/* This header is separate from features.h so that the compiler can
   include it implicitly at the start of every compilation.  It must
   not itself include <features.h> or any other header that includes
   <features.h> because the implicit include comes before any feature
   test macros that may be defined in a source file before it first
   explicitly includes a system header.  GCC knows the name of this
   header in order to preinclude it.  */

/* glibc's intent is to support the IEC 559 math functionality, real
   and complex.  If the GCC (4.9 and later) predefined macros
   specifying compiler intent are available, use them to determine
   whether the overall intent is to support these features; otherwise,
   presume an older compiler has intent to support these features and
   define these macros by default.  */
/* wchar_t uses Unicode 8.0.0.  Version 8.0 of the Unicode Standard is
   synchronized with ISO/IEC 10646:2014, plus Amendment 1 (published
   2015-05-15).  */


/* We do not support C11 <threads.h>.  */

/** @brief Struct which contains real time measurments. This struct cannot be queried but only be stream directly by connecting to the respective port. @ingroup romulus_structs */ 
typedef struct _romulus_packed {

int64_t TimeStamp; /**< Timestamp of the specific sample measured in miliseconds since POSIX epoch. */
int32_t ElapsedTimeInt1; /**< Elapsed time of integral one. The unit is the same as romulus_params_t#Integral1Time and it is always bigger or equal to 0 and smaller than romulus_params_t#Integral1Time. */
int32_t ElapsedTimeInt2; /**< Elapsed time of integral two. The unit is the same as romulus_params_t#Integral2Time and it is always bigger or equal to 0 and smaller than romulus_params_t#Integral2Time. */
int32_t MSID; /**< Measurment ID where the sample was taken (specified by romulus_params_t#MSID). */
float RawValue; /**< Measured signal in amperes. */
float BaseValue; /**< Measured dose rate. */
float AvgValue; /**< Average value of the dose which is calculated with the algorithm as specified by romulus_params_t#AvgAlgorithm. */
float Integral1; /**< Integral of the dose rate over a specified time (set by romulus_params_t#Integral1Time). */
float Integral2; /**< Integral of the dose rate over a specified time (set by romulus_params_t#Integral2Time). */
float MinValue; /**< Base values filtered with an exponential filter (specified by romulus_params_t#MinTimeCoeff). */
float MaxValue; /**< Base values filtered with an exponential filter (specified by romulus_params_t#MaxTimeCoeff). */
float Temperature; /**< Temperature (in °C) measured every romulus_params_t#THTime inside the measurment device close to the ionisation chamber. */
float Temperature2; /**< Temperature in °C measured every romulus_params_t#THTime inside the device by another sensor than for romulus_hist_data_t#Temperature. */
float CaseTemp; /**< Temperature of the case (measured every romulus_params_t#THTime in °C). */
float Humidity; /**< Relative humidity measured every romulus_params_t#THTime inside the measurment device. */

uint8_t MeasUnit; /**< Measurment unit: Sv/h when 0x01, Gr/h when 0x02 and set by romulus_params_t#MeasUnit. */
uint8_t Mode; /**< Mode of the measurment device. For more information, see romulus_params_t#Mode. */

bool Alert; /**< Defines if there was an alert. */
bool Alarm; /**< Defines if there was an alarm. */
bool SpecialAlarm; /**< Defines if there was a special alarm. */
bool MinorFault; /**< Defines if there was a minor fault. */
bool MajorFault; /**< Defines if there was a major fault. */
bool PersistentFault; /**< Defines if there was a persistant fault. */

bool SensorConnection; /**< True when there is a chamber attached and false if not. */
bool PowerSupply; /**< True when the powersupply (CUPS) is ok and false if there is an issue. */
bool Battery; /**< True if the battery is ok and charged and false if there is an issue with the battery. */
bool Charger; /**< Charger Comments to be done */

bool Input1; /**< Input1 state. */
bool Input2; /**< Input2 state. */
bool Input3; /**< Input3 state. */
bool Input4; /**< Input4 state. */
bool Output1; /**< Output1 state. */
bool Output2; /**< Output2 state. */
bool Output3; /**< Output3 state. */
bool Output4; /**< Output4 state. */

} romulus_rtmeas_t;
