#ifndef ROMULUS_INTERNALS_H
#define ROMULUS_INTERNALS_H

#include <stdio.h>

#include "romulus.h"

/* Defining the thread_local keyword for different versions of compilers (C or C++) */
#ifndef thread_local
    #define thread_local _Thread_local
#endif /* thread_local */
#if !defined(_Thread_local) && defined(__GNUC__)
    #define _Thread_local __thread
#else /* !defined(_Thread_local) && defined(__GNUC__) */
    #error "Bad compiler version and standard iteration"
#endif /* !defined(_Thread_local) && defined(__GNUC__) */

/**
 * @brief Internal, per thread storage location of the actual romulus debug level.
 * @ingroup romulus_debug
 */
extern thread_local romulus_log_level_t _romulus_log_level;

/**
 * @brief MAJOR_VERSION Major version of the library. This variable should never be written.
 * @ingroup romulus_version
 */
extern const unsigned int MAJOR_VERSION;
/**
 * @brief MINOR_VERSION Minor version of the library. This variable should never be written.
 * @ingroup romulus_version
 */
extern const unsigned int MINOR_VERSION;
/**
 * @brief COMMIT_ID Commit ID of the build of the library. This variable should never be written.
 * @ingroup romulus_version
 */
extern const unsigned int COMMIT_ID;

/**
 * @brief Abstract macro definition for logging.
 * @ingroup romulus_debug
 * @param L Log level.
 * @param S Display text such as ERROR, WARN, INFO or DEBUG (or others).
 * @param M Log message.
 * @param ... Varargs to be printed as log.
 */
#define romulus_log(L,S,M, ...) if(L.level <= _romulus_log_level.level){\
    char buff[25]; romulus_timestamp_str(romulus_timestamp_now(), buff);\
    fprintf((L.level <= ROMULUS_LOG_LEVEL_ERROR.level ? stderr : stdout), "[%s: %s ROMULUS]\t%s:%d:%s " M "\n",\
    buff, S, __FILE__, __LINE__, __FUNCTION__, ##__VA_ARGS__);\
    }

/**
 * @brief Logging for errors.
 * @ingroup romulus_debug
 * @param M Message to be printed as log.
 * @param ... Varargs to be printed as log.
 */
#define romulus_log_err(M, ...) romulus_log(ROMULUS_LOG_LEVEL_ERROR, "ERROR ", M, ##__VA_ARGS__)
/**
 * @brief Logging for warnings.
 * @ingroup romulus_debug
 * @param M Message to be printed as log.
 * @param ... Varargs to be printed as log.
 */
#define romulus_log_warn(M, ...) romulus_log(ROMULUS_LOG_LEVEL_WARNING, "WARN  ", M, ##__VA_ARGS__)
/**
 * @brief Logging for information.
 * @ingroup romulus_debug
 * @param M Message to be printed as log.
 * @param ... Varargs to be printed as log.
 */
#define romulus_log_info(M, ...) romulus_log(ROMULUS_LOG_LEVEL_INFO, "INFO  ", M, ##__VA_ARGS__)
/**
 * @brief Logging for debugging.
 * @ingroup romulus_debug
 * @param M Message to be printed as log.
 * @param ... Varargs to be printed as log.
 */
#define romulus_log_debug(M, ...) romulus_log(ROMULUS_LOG_LEVEL_DEBUG, "DEBUG ", M, ##__VA_ARGS__)

/**
 * @defgroup romulus_local_params Declarations for romulus local paramters.
 * Struct, variable and function definition for romulus local paramters.
 * @{
 */

#include "_romulus_local_params_struct.h"

/**
 * @brief _romulus_local_params_type_info Look-up table for type information for romulus local paramters (equavalent to _romulus_params_type_info for romulus_params_t).
 */
extern const romulus_type_info_t _romulus_local_params_type_info[sizeof(romulus_local_params_t)];
/**
 * @brief _romulus_local_params_type_info_by_id Look-up table for romulus type information for romulus local paramters when querying using the type ID (equivalent to _romulus_params_type_info_by_id for romulus_params_t).
 */
extern const romulus_type_info_t* _romulus_local_params_type_info_by_id[(1 << 8) * sizeof(uint8_t)];
/**
 * @brief ROMULUS_LOCAL_PARAMETERS_COUNT Number of romulus local paramters.
 */
extern const unsigned int ROMULUS_LOCAL_PARAMETERS_COUNT;
/** @} */

/**
 * @defgroup romulus_his_int Declarations for internal historics.
 * Struct definition for romulus internal historics.
 * @{
 */

#include "_romulus_hist_int_struct.h"

/** @} */

/**
 * @defgroup romulus_io Definition of printing and IO functions.
 * Definition of printing and IO functions.
 * @{
 */

/**
 * @brief _romulus_print_param_info
 * @param info Type info array. This can be _romulus_params_type_info, _romulus_status_type_info, _romulus_rtmeas_type_info, _romulus_hist_data_type_info, _romulus_hist_event_type_info, _romulus_hist_ts_type_info or _romulus_local_params_type_info.
 * @param length Length of the romulus type info array.
 * @param name_prefix Prefix to be printed in front of the field name, usually the struct name or an empty string.
 */
void _romulus_print_param_info(const romulus_type_info_t* const info, const unsigned int length, const char* const name_prefix);

/**
 * @brief romulus_params_write Write romulus or local paramters to a file stream. This function writes with the following format: paramter name + space + paramter value + linebreak in a single line and in the order of the fields.
 * @param fd Filestream pointer to write to: can be a file or stdout for example.
 * @param romulus_struct Pointer to the paramters structure to be written. This can either be of type romulus_params_t or romulus_local_params_t and the type must be coherent with the romulus_param_type_info parameter of the function.
 * @param romulus_struct_type_info Pointer to the paramters info array. It can either be _romulus_params_type_info, _romulus_local_params_type_info, or a pointer to one element of either array.
 * @param num_elements_to_print Number of elements of the array to print: either the full size or 1 when just one element of the array is desiered.
 * @return Returns 0 on success and -1 on error.
 */
int romulus_struct_write(FILE* fd, const void * const romulus_struct, const romulus_type_info_t* const romulus_struct_type_info, const unsigned int num_elements_to_print);
/**
 * @brief romulus_params_read Read romulus or local paramters from a text stream. This function expects the format of having the paramter name + space + paramter value + linebreak in a single line and in the order of the fields.
 * @param fd Filestream pointer to read from: can be a file or stdin for example.
 * @param params Pointer to the paramters structure to be written. This can either be of type romulus_params_t or romulus_local_params_t and the type must be coherent with the romulus_param_type_info parameter of the function.
 * @param romulus_param_type_info Pointer to the paramters info array. It can either be _romulus_params_type_info or _romulus_local_params_type_info.
 * @return Returns 0 on success and -1 on error.
 */
int romulus_params_read(FILE* fd, void * const params, const romulus_type_info_t* const romulus_param_type_info);
/** @} */

/**
 * @defgroup romulus_struct_internals Definition of internal data structures.
 * Definition of internal data structures.
 * @{
 */

/**
 * @brief Resolves the data type to a predefined symbol for single point data types.
 */
#define _ROMULUS_DATA_TYPE_RESOLVE0d(_type) _ROMULUS_DATA_TYPE_ ## _type
/**
 * @brief Resolves the data type to a predefined symbol for array-like data types.
 */
#define _ROMULUS_DATA_TYPE_RESOLVE1d(_type, _dim0) _ROMULUS_DATA_TYPE_RESOLVE0d(_type ## _ ## _dim0)
/**
 * @brief Resolves the data type to a predefined symbol for matrix-like data types.
 */
#define _ROMULUS_DATA_TYPE_RESOLVE2d(_type, _dim0, _dim1) _ROMULUS_DATA_TYPE_RESOLVE1d(_type ## _ ## _dim0, _dim1)

/**
 * @brief _romulus_status_type_info Look-up table to get type information for the real-time measurement struct.
 */
extern const romulus_type_info_t _romulus_rtmeas_type_info[sizeof(romulus_rtmeas_t)];
/**
 * @brief _romulus_status_type_info Look-up table to get type information for the historic data struct.
 */
extern const romulus_type_info_t _romulus_hist_data_type_info[sizeof(romulus_hist_data_t)];
/**
 * @brief _romulus_status_type_info Look-up table to get type information for the historic events struct.
 */
extern const romulus_type_info_t _romulus_hist_event_type_info[sizeof(romulus_hist_event_t)];
/**
 * @brief _romulus_status_type_info Look-up table to get type information for the historic events struct.
 */
extern const romulus_type_info_t _romulus_hist_int_type_info[sizeof(romulus_hist_int_t)];
/**
 * @brief _romulus_status_type_info Look-up table to get type information for the historic timestamps struct.
 */
extern const romulus_type_info_t _romulus_hist_ts_type_info[sizeof(romulus_hist_ts_t)];

/**
 * @brief _romulus_param_marshal Wrapper for ordinary marshalling (without any transformation of data) of romulus structures.
 * @param src Source of the data to be marshalled (usually within a romulus structure).
 * @param dest Destination where to marshal the data to (for example to a romulus frame).
 * @param info Pointer to the type info struct for the paramter which has to be marshalled.
 * @return Returns NULL on error or a valid pointer on success.
 */
const void* _romulus_param_marshal(const void* const src, void* const dest, const romulus_type_info_t * const info);
/**
 * @brief _romulus_param_unmarshal Wrapper for ordinary unmarshalling (without any transformation of data) of romulus structures.
 * @param src Source of the data to be unmarshalled (usually within a romulus frame).
 * @param dest Destination where to unmarshal the data to (for example to a romulus structure).
 * @param info Pointer to the type info struct for the paramter which has to be unmarshalled.
 * @return Returns NULL on error or a valid pointer on success.
 */
const void* _romulus_param_unmarshal(const void* const src, void* const dest, const romulus_type_info_t * const info);

/**
 * @brief _romulus_param_marshal_bool_11_40 Marshalls the coefficients matrix.
 * @param src Source of the coefficients matrix (usually inside a structure).
 * @param dest Destination of the coefficient matrix (usually inside a romulus frame).
 * @return Returns NULL on error or a valid pointer on success.
 */
const void* _romulus_param_marshal_bool_11_40(const void* const src, void* const dest);
/**
 * @brief _romulus_param_unmarshal_bool_11_40 Unmarshalls the coefficients matrix.
 * @param src Source of the coefficients matrix (usually inside a romulus frame).
 * @param dest Destination of the coefficients matrix (usually inside a romulus structure).
 * @return Returns NULL on error or a valid pointer on success.
 */
const void* _romulus_param_unmarshal_bool_11_40(const void* const src, void* const dest);
/** @} */

/**
 * @defgroup romulus_crc Declarations romulus CRC.
 * Struct, variable and function definition for romulus CRC.
 * @{
 */

/**
 * @brief The value of the generator polynomial for the CCITT CRC16 algorithm.
 */
#define CRC_16_CCITT_POLYNOMIAL 0x1021
/**
 * @brief The intial value of the CCITT CRC16 algorithm.
 */
#define CRC_16_CCITT_INIT_VALUE 0xffff
/**
 * @brief The size of the look-up table for the CCITT CRC16 algorithm.
 */
#define CRC_16_CCITT_TABLE_SIZE 0x100

/**
 * @brief crc_16_ccitt_table Look-up table for the CCITT CRC16 algorithm.
 */
extern uint16_t crc_16_ccitt_table[CRC_16_CCITT_TABLE_SIZE];

/**
 * @brief _romulus_crc_init Initialises the look-up table (crc_16_ccitt_table) for the CCITT CRC16 algorithm.
 */
void _romulus_crc_init(void);
/**
 * @brief _romulus_crc Calculates the CCITT CRC16 algorithm over a given buffer.
 * @param buffer Pointer to the data over which the CRC has to be calculated.
 * @param size Size of the buffer over which the CRC is calcualted.
 * @return Returns the CRC checksum of the buffer.
 */
uint16_t _romulus_crc(const void* const buffer, const size_t size);
/** @} */

/** @defgroup romulus_misc_utility Definition of some utlity macros.
 * Definition of some utlity macros.
 * @{
 */

/**
 * @brief Macro to determine the size of a field in a struct.
 * @param _type_name Name of the struct.
 * @param _field_name Name of the field of the struct.
 * @return Return the size of a field of a struct.
 */
#define size_of_type(_type_name, _field_name) sizeof(((_type_name*)NULL)->_field_name)

/**
 * @brief Resolves the data type using the C11 Generic mechanism. This mechanism is not used yet but is there, so that it can be used as soon as a compiler which supports it is available on CERN CentOS.
 * @param _type Type to be resolved.
 * @return Returns a symbol to identify the type.
 */
#define ROMULUS_RESOLVE_TYPE(_type) _Generic((_type),\
    _Bool:          ROMULUS_DATA_TYPE_bool,\
    uint8_t:        ROMULUS_DATA_TYPE_uint8_t,\
    int32_t:        ROMULUS_DATA_TYPE_int32_t,\
    int64_t:        ROMULUS_DATA_TYPE_int64_t,\
    float:          ROMULUS_DATA_TYPE_float,\
    uint8_t[16]:    ROMULUS_DATA_TYPE_uint8_t_16,\
    uint8_t[64]:    ROMULUS_DATA_TYPE_uint8_t_64,\
    _Bool[11][40]:  ROMULUS_DATA_TYPE_bool_11_40,\
    default:        ROMULUS_DATA_TYPE_undef)
/** @} */

#endif /* ROMULUS_INTERNALS_H */
