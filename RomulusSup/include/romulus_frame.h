#ifndef ROMULUS_FRAME_H
#define ROMULUS_FRAME_H

#include <stdint.h>
#include <sys/types.h>
#include <stdio.h>

#include "romulus.h"
#include "romulus_internals.h"

/** @defgroup frame_structure Romulus frame structure definitions
 *  All data types and constants related to romulus frames are defined here
 *  @{
 */

/**
 * @brief romulus_frame_t Romulus frame type definition
 *
 * All romulus frames share the structure as shown below *
 * --------------------------------------------
 * | Header | Data (variable length) | Footer |
 * --------------------------------------------
 */
typedef void romulus_frame_t;

/**
 * @brief Wrapper structure for response codes sent as a response to request.
 */
typedef struct{
    uint8_t code; /**< Actual code value, which is one byte big. */
} romulus_response_code_t;
static_assert(sizeof(romulus_response_code_t) == sizeof(uint8_t), "Romulus response codes must 1 byte wide.");

/**
 * @brief Response code sent when the request was executed succesfully and the reponse data is valid.
 */
#define ROMULUS_RESPONSE_CODE_ACKNOWLEDGE           ((const romulus_response_code_t){0x99})
/**
 * @brief Response code sent when a request contains an unknown/invalid command code.
 */
#define ROMULUS_RESPONSE_CODE_ILLEGAL_COMMAND       ((const romulus_response_code_t){0x01})
/**
 * @brief Response code sent when a paramter set with ROMULUS_COMMAND_CODE_SET_PARAM_REQUEST is non-setable or unknown.
 */
#define ROMULUS_RESPONSE_CODE_ILLEGAL_PARAMETER     ((const romulus_response_code_t){0x02})
/**
 * @brief Response code sent when a paramter set with ROMULUS_COMMAND_CODE_SET_PARAM is out-of-range.
 */
#define ROMULUS_RESPONSE_CODE_ILLEGAL_VALUE         ((const romulus_response_code_t){0x03})
/**
 * @brief Response code sent when an action is attempted which cannot be executed in the current mode ( romulus_params_t#Mode ).
 */
#define ROMULUS_RESPONSE_CODE_ILLEGAL_MODE          ((const romulus_response_code_t){0x04})
/**
 * @brief Response code sent when a malformed ROMULUS_COMMAND_CODE_SET_PARAM_REQUEST or ROMULUS_COMMAND_CODE_SET_LOCAL_PARAM_REQUEST frame is received for setting
 * paramters where the number of parameters in the header ( romulus_frame_header_t#np ) and the number of
 *  parameters contained in the frames data section are contradicting.
 */
#define ROMULUS_RESPONSE_CODE_INCONSISTANT_FRAME    ((const romulus_response_code_t){0x05})
/**
 * @brief Response code sent when a ROMULUS_COMMAND_CODE_SET_PARAM_REQUEST or ROMULUS_COMMAND_CODE_SET_LOCAL_PARAM_REQUEST frame tries to set the same paramter mulitple times.
 */
#define ROMULUS_RESPONSE_CODE_REPEATED_PARAMETER    ((const romulus_response_code_t){0x06})
/**
 * @brief Response code sent when a device encounter an internal problem which rends it incapable of
 * handling the request.
 */
#define ROMULUS_RESPONSE_CODE_FAILURE               ((const romulus_response_code_t){0x07})
/**
 * @brief This response code should never be actually transmitted and is used for internal purpose only.
 */
#define ROMULUS_RESPONSE_CODE_FUNCTION_FAILURE      ((const romulus_response_code_t){0x08})

/**
 * @brief Wrapper structure for the romulus command code used in the romulus frame header ( romulus_frame_header_t#cmd_code).
 */
typedef struct{
    uint16_t command_code; /**< Actual code, which must be two bytes wide. */
} romulus_command_code_t;
static_assert(sizeof(romulus_command_code_t) == sizeof(uint16_t), "Romulus command codes must 2 byte wide.");

/**
 * @brief Command code used in request frames to set romulus paramters.
 */
#define ROMULUS_COMMAND_CODE_SET_PARAM_REQUEST                  ((const romulus_command_code_t){0x0001})
/**
 * @brief Command code used in response frames to set romulus paramters.
 */
#define ROMULUS_COMMAND_CODE_SET_PARAM_RESPONSE                 ((const romulus_command_code_t){0x0002})
/**
 * @brief Command codes used in request frames to read romulus paramters.
 */
#define ROMULUS_COMMAND_CODE_GET_PARAM_REQUEST                  ((const romulus_command_code_t){0x0003})
/**
 * @brief Command codes used in response frames to read romulus paramters.
 */
#define ROMULUS_COMMAND_CODE_GET_PARAM_RESPONSE                 ((const romulus_command_code_t){0x0004})
/**
 * @brief Command code used in request frames to read the romulus status of a device.
 */
#define ROMULUS_COMMAND_CODE_STATUS_REQUEST                     ((const romulus_command_code_t){0x0005})
/**
 * @brief Command code used in request frames to read the romulus status of a device.
 */
#define ROMULUS_COMMAND_CODE_STATUS_RESPONSE                    ((const romulus_command_code_t){0x0006})
/**
 * @brief Command code used for request frames to query for timestamp of historic data.
 */
#define ROMULUS_COMMAND_CODE_HIST_TIMESTAMPS_DATA_REQUEST       ((const romulus_command_code_t){0x0007})
/**
 * @brief Command code used for response frames to queries for timestamp of historic data.
 */
#define ROMULUS_COMMAND_CODE_HIST_TIMESTAMPS_DATA_RESPONSE      ((const romulus_command_code_t){0x0008})
/**
 * @brief Command code used for request frames to query for timestamp of historic events.
 */
#define ROMULUS_COMMAND_CODE_HIST_TIMESTAMPS_EVENTS_REQUEST     ((const romulus_command_code_t){0x0009})
/**
 * @brief Command code used for response frames to queries for timestamp of historic events.
 */
#define ROMULUS_COMMAND_CODE_HIST_TIMESTAMPS_EVENTS_RESPONSE    ((const romulus_command_code_t){0x000a})
/**
 * @brief Command code used in request frames to set local paramters.
 */
#define ROMULUS_COMMAND_CODE_SET_LOCAL_PARAM_REQUEST            ((const romulus_command_code_t){0x000b})
/**
 * @brief Command code used in response frames to set local paramters.
 */
#define ROMULUS_COMMAND_CODE_SET_LOCAL_PARAM_RESPONSE           ((const romulus_command_code_t){0x000c})
/**
 * @brief Command codes used in request frames to read local paramters.
 */
#define ROMULUS_COMMAND_CODE_GET_LOCAL_PARAM_REQUEST            ((const romulus_command_code_t){0x000d})
/**
 * @brief Command codes used in response frames to read local paramters.
 */
#define ROMULUS_COMMAND_CODE_GET_LOCAL_PARAM_RESPONSE           ((const romulus_command_code_t){0x000e})
/**
 * @brief Command code used for request frames to queries for timestamp of historic internal data.
 */
#define ROMULUS_COMMAND_CODE_HIST_TIMESTAMPS_INT_REQUEST        ((const romulus_command_code_t){0x000f})
/**
 * @brief Command code used for response frames to queries for timestamp of historic internal data.
 */
#define ROMULUS_COMMAND_CODE_HIST_TIMESTAMPS_INT_RESPONSE       ((const romulus_command_code_t){0x0010})
/**
 * @brief Command code used to request historic data (only used in requests).
 */
#define ROMULUS_COMMAND_CODE_HIST_DATA_REQUEST                  ((const romulus_command_code_t){0x0100})
/**
 * @brief Command code used in historic data response frame (e.g. exclusively used in responses).
 * This code can only come after a ROMULUS_COMMAND_CODE_HIST_DATA_REQUEST and is always followd by
 * ROMULUS_COMMAND_CODE_HIST_DONE_RESPONSE.
 */
#define ROMULUS_COMMAND_CODE_HIST_DATA_RESPONSE                 ((const romulus_command_code_t){0x0200})
/**
 * @brief Command code used to request historic events (only used in requests).
 */
#define ROMULUS_COMMAND_CODE_HIST_EVENTS_REQUEST                ((const romulus_command_code_t){0x0300})
/**
 * @brief  Command code used in historic events response frame (e.g. exclusively used in responses).
 * This code can only come after a ROMULUS_COMMAND_CODE_HIST_EVENTS_REQUEST and is always followd by
 * ROMULUS_COMMAND_CODE_HIST_DONE_RESPONSE.
 */
#define ROMULUS_COMMAND_CODE_HIST_EVENTS_RESPONSE               ((const romulus_command_code_t){0x0400})
/**
 * @brief Command code sent as a last response after a historic query has finished. This code is always used after
 * ROMULUS_COMMAND_CODE_HIST_DATA_RESPONSE or ROMULUS_COMMAND_CODE_HIST_EVENTS_RESPONSE, or
 * ROMULUS_COMMAND_CODE_HIST_DATA_REQUEST or ROMULUS_COMMAND_CODE_HIST_EVENTS_REQUEST when no data/events were available.
 */
#define ROMULUS_COMMAND_CODE_HIST_DONE_RESPONSE                 ((const romulus_command_code_t){0x0500})
/**
 * @brief Command code used to request historic internal data (only used in requests).
 */
#define ROMULUS_COMMAND_CODE_HIST_INT_REQUEST                   ((const romulus_command_code_t){0x0600})
/**
 * @brief Command code used in historic historic data response frame (e.g. exclusively used in responses).
 * This code can only come after a ROMULUS_COMMAND_CODE_HIST_INT_REQUEST and is always followd by
 * ROMULUS_COMMAND_CODE_HIST_DONE_RESPONSE.
 */
#define ROMULUS_COMMAND_CODE_HIST_INT_RESPONSE                  ((const romulus_command_code_t){0x0700})
/**
 * @brief Command code used to initialise real time measurment streaming.
 */
#define ROMULUS_COMMAND_CODE_RTMEAS_REQUEST                     ((const romulus_command_code_t){0x1000})
/**
 * @brief Command code used in real time measurment streaming response frames.
 */
#define ROMULUS_COMMAND_CODE_RTMEAS_RESPONSE                    ((const romulus_command_code_t){0x2000})
/**
 * @brief Command code used to indicate real time measurment streaming stopping (sent by device or supervision).
 */
#define ROMULUS_COMMAND_CODE_RTMEAS_STOP                        ((const romulus_command_code_t){0x3000})

/**
 * @brief ETHERNET_MTU Maximum length of a romulus frame including header + data + footer
 */
#define ETHERNET_MTU 1500

/**
 * @brief ROMULUS_ID_STR_LENGTH Number of characters of the ID string in the header of a romulus frame
 */
#define ROMULUS_ID_STR_LENGTH 16

/**
 * @brief Structure definition of a romulus frame header
 */
typedef struct _romulus_packed {
    uint16_t dlc; /**< Number of bytes in the data section of the frame (which is variable) */
    uint8_t  id_source[ROMULUS_ID_STR_LENGTH]; /**< Identifier of who sent the frame */
    uint16_t seq_id; /**< Sequence ID number of the frame */
    romulus_command_code_t cmd_code; /**< Command code */
    uint8_t  np; /**< Number of paramters which are in the frame */
} romulus_frame_header_t;
static_assert(sizeof(romulus_frame_header_t) == 23, "ROMULUS frame header size is invalid");
static_assert(offsetof(romulus_frame_header_t, dlc) == 0, "ROMULUS frame header is invalid");
static_assert(offsetof(romulus_frame_header_t, id_source) == 2, "ROMULUS frame header is invalid");
static_assert(offsetof(romulus_frame_header_t, seq_id) == 18, "ROMULUS frame header is invalid");
static_assert(offsetof(romulus_frame_header_t, cmd_code) == 20, "ROMULUS frame header is invalid");
static_assert(offsetof(romulus_frame_header_t, np) == 22, "ROMULUS frame header is invalid");

/**
 * @brief Structure definition of a romulus frame footer
 */
typedef struct _romulus_packed {
    uint16_t chksum; /**< CRC-16 checksum over the complete frame up to here (header + data) */
} romulus_frame_footer_t;
static_assert(sizeof(romulus_frame_footer_t) == 2, "ROMULUS frame footer size is invalid");
static_assert(offsetof(romulus_frame_footer_t, chksum) == 0, "ROMULUS frame footer is invalid");

/**
 * @brief _local_id Storage location of the local ID. This should only be accessed by functions and never directly by the user.
 */
extern char _local_id[ROMULUS_ID_STR_LENGTH];
/** @} */

/** @defgroup frame_lifecycle Frame lifecycle functions
 *  All function required to define the lifecycle of a frame.
 *  @{
 */

/**
 * @brief _romulus_frame_alloc Allocates a new frame.
 * @return Return NULL when allocation failed or a pointer to the newly allocated frame.
 */
romulus_frame_t* _romulus_frame_alloc();
/**
 * @brief _romulus_frame_init Initialises the header of a frame.
 * @param frame Pointer to the frame to be initialised.
 * @param sequence_id Desired sequence ID of the frame.
 * @param command Command or type of the frame.
 * @return Returns NULL on error or a pointer to the frame on success.
 */
romulus_frame_t* _romulus_frame_init(romulus_frame_t* const frame, const uint16_t sequence_id, const romulus_command_code_t command);
/**
 * @brief romulus_frame_alloc_init Allocates and initilises a frame.
 * @param sequence_id Desired sequence ID of the frame.
 * @param command Type of frame.
 * @return Returns NULL on error or pointer to the allocated and initialised frame.
 */
romulus_frame_t* romulus_frame_alloc_init(const uint16_t sequence_id, const romulus_command_code_t command);
/**
 * @brief romulus_frame_generate_sequence_id Generates a new sequence ID for a frame based on its previous sequence ID. The sequence ID must
 * have been initialised prior to being used.
 * @param sequence_id Pointer to the previous sequence ID which will be altered by the call of this function.
 * @return Returns always 0 if the sequence_id is a NULL pointer or a valid sequence ID when sequence_id is pointing to a valid memory address.
 */
uint16_t romulus_frame_generate_sequence_id(uint16_t* const sequence_id);
/**
 * @brief romulus_frame_free Frees the memory occupied by a frame. It cannot be used any more after being freed.
 * @param frame Pointer to the frame to be freed.
 */
void romulus_frame_free(romulus_frame_t* const frame);
/** @} */

/** @defgroup frame_accesors Frame accessor functions
 *  All function required to access parts of frames.
 *  @{
 */

/**
 * @brief romulus_frame_read_header Returns a pointer to the header of a frame to be read from.
 * @param frame Pointer to the frame of which the header has to be read.
 * @return Returns NULL on error or a valid pointer to the header of the frame.
 */
const romulus_frame_header_t* romulus_frame_read_header(const romulus_frame_t* const frame);
/**
 * @brief _romulus_frame_read_data_with_offset Returns a pointer to a certain offset to read from withing the data segment of a frame.
 * @param frame Pointer of the frame to read from.
 * @param offset Offset within the data segment where one wants to read.
 * @return Returns NULL on error or a valid pointer to the data at the given offset within the data segment of the frame.
 */
const void* _romulus_frame_read_data_with_offset(const romulus_frame_t* const frame, const uint16_t offset);
/**
 * @brief romulus_frame_read_response_code Returns the response code of a romulus response frame
 * @param frame Pointer to the romulus response frame of which the return code has to be read.
 * @return Return ROMULUS_RESPONSE_CODE_FUNCTION_FAILURE on error or the actual response code of the frame.
 */
romulus_response_code_t romulus_frame_read_response_code(const romulus_frame_t* const frame);
/**
 * @brief romulus_frame_read_footer Returns a pointer to the footer of a frame to read from.
 * @param frame Pointer to the frame of which the footer has to be read.
 * @return Returns NULL on error or a valid pointer to the footer of the frame on success.
 */
const romulus_frame_footer_t* romulus_frame_read_footer(const romulus_frame_t* const frame);
/**
 * @brief _romulus_frame_get_header Returns a pointer to the header of a frame to read from and write to.
 * @param frame Pointer to the frame of which the header has to be read/written.
 * @return Returns NULL on error or a valid pointer to the header of the frame.
 */
romulus_frame_header_t* _romulus_frame_get_header(romulus_frame_t* const frame);
/**
 * @brief _romulus_frame_append_data Appends data to a frame, e.g. adds the actual data to the data segment and increments the data coutner
 * in the header accordingly.
 * @param frame Pointer to the frame to which data has to be added.
 * @param data Pointer to the actual data which has to be added.
 * @param dataLength Size of the data to be added.
 * @return Returns NULL on error or the pointer to the frame on success.
 */
romulus_frame_t* _romulus_frame_append_data(romulus_frame_t* const frame, const void* const data, const size_t dataLength);
/**
 * @brief _romulus_frame_append_param_data
 * @param frame
 * @param data
 * @param info
 * @return
 */
romulus_frame_t* _romulus_frame_append_param_data(romulus_frame_t* const frame, const void* const data, const romulus_type_info_t * const info);
/**
 * @brief _romulus_frame_append_footer Appends the footer (CRC and termination sequence) to a frame.
 * @param frame Pointer to the frame where the footer and the termination sequence has to be added.
 * @return Returns NULL on error or a valid pointer to the frame on success.
 */
romulus_frame_t *_romulus_frame_append_footer(romulus_frame_t * const frame);
/**
 * @brief romulus_frame_get_size Returns the size of a given frame.
 * @param frame Pointer to the frame of which the current size is queried.
 * @return Returns -1 on error or the actual size (greater or equal to zero) on success.
 */
ssize_t romulus_frame_get_size(const romulus_frame_t * const frame);
/**
 * @brief _romulus_frame_get_free_space Returns the available space within a frame.
 * @param frame Pointer to the frame of which the free space is queried.
 * @return Returns -1 on erro or the actual free (greater or equal to zero) on success.
 */
ssize_t _romulus_frame_get_free_space(const romulus_frame_t* const frame);
/**
 * @brief romulus_frame_check_crc Checks the CRC of a given frame.
 * @param frame Pointer to the frame where the CRC has to be checked.
 * @return Returns false when 1. the CRC failed or 2. the frame pointer is NULL or true when the CRC check succeeded.
 */
bool romulus_frame_check_crc(const romulus_frame_t* const frame);
/** @} */

/** @defgroup frame_request Frame request functions
 *  Functions which can be used to create requests
 *  @{
 */

/**
 * @brief romulus_frame_create_request Creates a request frame. This includes allocating memory for the request which
 * later has to be freed again using romulus_frame_free(romulus_frame_t* const).
 * @param sequence_id The desired sequence ID for the request frame. This should typically be generated with the Romulus function.
 * @param command Any type of command ID like ROMULUS_COMMAND_CODE_SET_PARAM_REQUEST for setting a paramter or ROMULUS_COMMAND_CODE_GET_PARAM_REQUEST for retrieving parameters
 * @return Returns NULL on error or a pointer to a frame which can be manipulated. The frame later has to be freed again using romulus_frame_free(romulus_frame_t* const).
 */
romulus_frame_t* romulus_frame_create_request(const uint16_t sequence_id, const romulus_command_code_t command);
/** @} */

/** @defgroup frame_params Parameter frame functions
 *  All functions which are used to manipulate/use paramter frames.
 *  @{
 */

/**
 * @brief romulus_frame_param_add_param_to_request Manipluates a romulus request frame to add parameters which are to be set or retrieved.
 * @param request A pointer to the frame to which the paramter has to be added.
 * @param param_info Parameter ID of the parameter which has to be added to the frame.
 * @param value_ptr When the type of the request is to set a parameter, then this is a pointer to the actual data value to be added. Its size
 * is determined by using the param_id. When the request is the retrieve paramters, then this argument is ignored and can be set to any value (including NULL).
 * @return NULL on error or a pointer to the input request frame.
 */
romulus_frame_t* romulus_frame_param_add_param_to_request(romulus_frame_t* const request, const romulus_type_info_t* const param_info, const void* const value_ptr);

/**
 * @brief romulus_frame_param_check_request Checks if a request frame is valid. This function should only be used on the CROME side, not on the Supervision side. TODO: make the function work on both sides
 * @param request A pointer to the request frame which has to be checked.
 * @param params_protection A pointer to the paramter protection mask. The mask is only checked when the type of frame is ROMULUS_CMD_SetParam, otherwise
 * this pointer can contain any information (including NULL) as it is ignored.
 * @return Returns ROMULUS_RESPONSE_CODE_FUNCTION_FAILURE on error or any code defined by romulus_response_code_t on succcessful execution (including when the frame is invalid, but the code yields this informantion).
 */
romulus_response_code_t romulus_frame_param_check_request(const romulus_frame_t* const request, const romulus_param_protection_t* const params_protection);

/**
 * @brief romulus_frame_param_create_response Creates a response frame to a request which affects the parametrisation. This includes allocating
 * memory for the request which later has to be freed again using romulus_frame_free(romulus_frame_t* const).
 * This function internally calls romulus_frame_param_check_request(const romulus_frame_t* const, const romulus_param_protection_t* const)
 * To verify the validity of the request. This function does not load the frame content to the parameters struct, for this
 * omulus_frame_param_write_frame_to_struct(const romulus_frame_t* const, romulus_params_t* const) has to be called.
 * @param request A pointer to the request which has to be responded.
 * @param params_protection A pointer to the paramter protection mask. This value is only used when the request is of type ROMULUS_CMD_SetParam. If the type is differnt,
 * any value including NULL can be used as it is ignored.
 * @param params A pointer to the paramters, which are either of type romulus_params_t or romulus_local_params_t.
 * @return Returns NULL on error or a pointer to a frame which has to be sent. The frame later has to be freed again using romulus_frame_free(romulus_frame_t* const).
 */
romulus_frame_t* romulus_frame_param_create_response(const romulus_frame_t* const request, const romulus_param_protection_t* const params_protection, const void* const params);

/**
 * @brief romulus_frame_param_write_frame_to_struct Updates the paramtrisation according to the content of the romulus frame. This function can only be used after
 * the input frame has been validated with romulus_frame_param_check_request(const romulus_frame_t* const, const romulus_param_protection_t* const) (or internally by
 * calling romulus_frame_param_create_response(const romulus_frame_t* const, const romulus_param_protection_t* const, const romulus_params_t* const) ) and the
 * function return values have been checked for errors (which can also be the content of the response frame if one is created). If the frame has not been processed previously
 * by these two functions, the usage of this function is unsave.
 * @param frame A pointer to the frame from which the parameters have to be loaded.
 * @param params A pointer to the structure (romulus_params_t or romulus_local_params_t) which is updated using the frame's content.
 * @param is_request True when the frame from which has to be read is a request and false if it is a response frame.
 * @return Returns NULL on error and a pointer to the processed frame on success.
 */
const romulus_frame_t* romulus_frame_param_write_frame_to_struct(const romulus_frame_t* const frame, void * const params, const bool is_request);
/** @} */

/** @defgroup frame_status Status frame functions
 *  All functions which are used to manipulate/use status frames
 *  @{
 */

/**
 * @brief romulus_frame_status_create_response Creates a response frame to a get status request frame. The reponse frame has to be freed again using
 * romulus_frame_free(romulus_frame_t* const).
 * @param request Pointer to the request frame.
 * @param status_var Pointer to the status of the device (needed to answer the request).
 * @return return Returns NULL on error or a valid pointer to a reponse frame (which later has to be freed again using romulus_frame_free(romulus_frame_t* const)).
 */
romulus_frame_t* romulus_frame_status_create_response(const romulus_frame_t* const request, const romulus_status_t* const status_var);
/**
 * @brief romulus_frame_status_write_frame_to_struct Updates the status using the data present in the get status reponse frame.
 * @param frame Pointer to the get status reponse frame.
 * @param status_var Pointer to the status struct which has to be updated using the data present in the reponse frame.
 * @return Returns NULL on error or a valid pointer to the input reponse frame.
 */
const romulus_frame_t* romulus_frame_status_write_frame_to_struct(const romulus_frame_t* const frame, romulus_status_t* const status_var);
/** @} */

/** @defgroup frame_hist Historical frame functions
 *  All functions which are used to manipulate/use historical frames
 *  @{
 */

/**
 * @brief romulus_frame_hist_timestamps_create_response Creates a response to a timestamp query frame.
 * @param request Pointer to the timestamp request frame.
 * @param timestamps_struct Pointer to the struct which contains all available timestamps. This struct can be generated with the function
 * romulus_hist_search_available_timestamps(const romulus_frame_t* const, romulus_hist_chk_ts_t* const, const char* const, const char* const, ts_buffer_t* const);
 * @return Returns NULL on error or a valid pointer to the generated reponse frame. This frame later must be freed again using romulus_frame_free(romulus_frame_t* const).
 */
romulus_frame_t* romulus_frame_hist_timestamps_create_response(const romulus_frame_t* const request, const romulus_hist_ts_t * const timestamps_struct);
/**
 * @brief romulus_frame_hist_timestamps_read_from_frame Reads the timestamps from a reponse frame to a request of the available timestamps.
 * @param frame Pointer to the response frame which contains the timestamps.
 * @param timestamps Pointer to the memory location of the timestamps struct where the timestamps in the frame have to be stored.
 * @return Returns NULL on error or a valid pointer to the timestamps struct.
 */
romulus_hist_ts_t *romulus_frame_hist_timestamps_read_from_frame(const romulus_frame_t* const frame, romulus_hist_ts_t* const timestamps);
/**
 * @brief romulus_frame_hist_search_set_request Sets the request (how to filter and over which timespan) for a historical data/events search request.
 * @param request Pointer to the historical data/events search frame where the search type/span has to be set.
 * @param hist_request Pointer to the struct which defines the type and/or the time span of the historical data/events search.
 * @return Returns NULL on error or a valid pointer to the modified search request frame.
 */
romulus_frame_t* romulus_frame_hist_search_set_request(romulus_frame_t* const request, const romulus_hist_ts_t* const hist_request);
/**
 * @brief romulus_frame_hist_search_create_response Creates a response frame containing a part or all queried data for a historical data/event search.
 * @param request Pointer to the request frame. The type of the request frame is used to determine the type of response_var.
 * @param record_index A counter which shall be incremented at any creation of a response frame within a single search and then be reset after
 * the search has nedded. It can be used by the receiving side to check if frames have been dropped.
 * @param response_var Pointer to the response struct. The actual type of it is determined using the type of the request frame.
 * The pointed space must be big enough to accomodate the whole variable as this function assumes this to be true.
 * @return Returns NULL on error or a valid pointer to the reponse frame. The reponse frame has to be freed again using romulus_frame_free(romulus_frame_t* const).
 */
romulus_frame_t* romulus_frame_hist_search_create_response(const romulus_frame_t* const request, const int32_t record_index, const void* const response_var);
/**
 * @brief romulus_frame_hist_search_read_from_frame Reads the historical data/events search results from search reponse frame.
 * @param response Pointer to the historical data/events search reponse frame.
 * @param hist_search_response_struct Pointer to the struct where the search results in the frame have to be stored. Its actual data type is
 * determined internally using the type of the frame and the pointed memory location must be big enough so that the corresponding reponse data
 * type can fit.
 * @return Returns -1 on error or the record index of the frame withing the search (which is in [0, INT32MAX]).
 */
int32_t romulus_frame_hist_search_read_from_frame(const romulus_frame_t* const response, void* const hist_search_response_struct);
/**
 * @brief romulus_frame_hist_search_create_done Creates a search done frame to end a historical data/events search.
 * @param request Pointer to the original request frame which is fully answered by sending the frame generated by this function.
 * @param response_code Romulus response code to indicate success or failure of the historic search.
 * @param num_sent_records How many records have been sent during the search.
 * @return Returns NULL on error or a valid pointer to the last reponse frame. This frame has to be freed again using romulus_frame_free(romulus_frame_t* const).
 */
romulus_frame_t* romulus_frame_hist_search_create_done(const romulus_frame_t* const request, const romulus_response_code_t response_code, const int32_t num_sent_records);
/**
 * @brief romulus_frame_hist_search_done_read_from_frame Reads how many records were sent in a historical search in the response done frame of the search.
 * @param response Pointer to the romulus hist search done frame.
 * @return Returns -1 on error or the number of records sent throughout the whole search process.
 */
int32_t romulus_frame_hist_search_done_read_from_frame(const romulus_frame_t* const response);
/** @} */

/** @defgroup frame_rtmeas Realtime-measurment frame functions
 *  All functions which are used to manipulate/use realtime-measurment frames
 *  @{
 */

/**
 * @brief romulus_frame_rtmeas_create_response Creates a real time measurment frame.
 * @param rtmeas_var Pointer to the real time measurment struct which has to be packed into the frame.
 * @param sequence_id Sequence ID of the frame.
 * @return Return NULL on error or a vlaid pointer to hte create real time measurment frame. This frame has to be freed again using romulus_frame_free(romulus_frame_t* const).
 */
romulus_frame_t* romulus_frame_rtmeas_create_response(const romulus_rtmeas_t * const rtmeas_var, const uint16_t sequence_id);
/**
 * @brief romulus_frame_rtmeas_write_frame_to_struct Writes the real time measurment data present in a frame to a real time measurment struct.
 * @param frame Pointer to the frame from where the real time measurment data can be read.
 * @param rtmeas_var Pointer to the real time measurment struct where the data present in the frame has to be stored to.
 * @return Returns NULL on error or a valid pointer to the frame on success.
 */
const romulus_frame_t*  romulus_frame_rtmeas_write_frame_to_struct(const romulus_frame_t* const frame, romulus_rtmeas_t* const rtmeas_var);
/** @} */

/** @defgroup frame_misc Frame misc functions
 *  All kind of utility/debug functions for frames
 *  @{
 */

/**
 * @brief romulus_frame_print_frame Prints the binary content of a frame in hex with some humand readable annotations.
 * @param frame Pointer to the frame to be printed.
 * @param fp File descriptor to write to. Can be a real I/O stream or a file descriptor pointing to some memory, but the memory needs to be
 * sufficiently large (>4KB (not verified, just a vague estimate)).
 */
void romulus_frame_print_frame(const romulus_frame_t* const frame, FILE* fp);
/** @} */

#endif // ROMULUS_FRAME_H
