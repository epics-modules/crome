
/* Copyright (C) 1991-2016 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */




/* This header is separate from features.h so that the compiler can
   include it implicitly at the start of every compilation.  It must
   not itself include <features.h> or any other header that includes
   <features.h> because the implicit include comes before any feature
   test macros that may be defined in a source file before it first
   explicitly includes a system header.  GCC knows the name of this
   header in order to preinclude it.  */

/* glibc's intent is to support the IEC 559 math functionality, real
   and complex.  If the GCC (4.9 and later) predefined macros
   specifying compiler intent are available, use them to determine
   whether the overall intent is to support these features; otherwise,
   presume an older compiler has intent to support these features and
   define these macros by default.  */
/* wchar_t uses Unicode 8.0.0.  Version 8.0 of the Unicode Standard is
   synchronized with ISO/IEC 10646:2014, plus Amendment 1 (published
   2015-05-15).  */


/* We do not support C11 <threads.h>.  */

/** @brief Romulus paramters struct, which is used to parametrise the system plus to read back some inherent parameters of the system. @ingroup romulus_structs */ 
typedef struct _romulus_packed {

uint8_t NetworkName[64]; /**< Read-only etwork name of the device. */
uint8_t TimeServer1[64]; /**< Read-only timeserver 1 name of the device. */
uint8_t TimeServer2[64]; /**< Read-only timeServer 2 name of the device. */

uint8_t Firmware[64]; /**< Read-only firmware identifier of the device. */
uint8_t Serial[16]; /**< Serial of the device. Copied from romulus_local_params_t#Serial and read-only in this struct. */
uint8_t FuncPosition[16]; /**< Functional position of the device. Taken from romulus_local_paramters_t#FuncPosition and read-only. */

int64_t DateTime; /**< Time of the device, in ms since POSIX epoch. */
int64_t ParameterTimeStamp; /**< Time of the last update of the paramtrisation in ms since POSIX epoch. */

int32_t Inventory; /**< Inventory number. */

int32_t IP; /**< IP address in its native 32 bit form of hte device (read-only). */
int32_t Subnet; /**< Subnet mask in its native 32 bit form of the device (read-only). */
int32_t DNS1; /**< IP adress represented by its native 32 bit form of the first DNS server (read-only). */
int32_t DNS2; /**< IP adress represented by its native 32 bit form of the second DNS server (read-only). */

int32_t FastestRefreshTime; /**< Minimal update period of the real-time measurments in multiples of 100ms [1, 864000] s. */
int32_t DeviceCycleTime; /**< Device cycle time is the periodicity (in miliseconds) of how frequently device internal paramters (such as low voltages) are being checked [0.1, 8640] s. */
int32_t MeasTime; /**< Measurement time is the periodicity of how frequently new measurments are done (in miliseconds) [1.0, 86400.0] s. */

int32_t Integral1Time; /**< Integral1Time Comments to be done */
int32_t Integral2Time; /**< Integral2Time Comments to be done */
float MinTimeCoeff; /**< Averaging time for the minimum value fault (exponential moving average algorithm) [0.00001, 0.99999]. */
float MaxTimeCoeff; /**< Averaging time for the maximum value fault (exponential moving average algorithm) [0.00001, 0.99999]. */
int32_t THTime; /**< Update period in seconds of how often temperature and humidity values are measured [1, 86400]. */
int32_t PowerTime; /**< Update period in seconds of how often the status of the power supply is checked [1, 86400]. */
int32_t CAUAttached; /**< Number of attached CAUs [0, 15]. */
float ConvFactor; /**< Conversion factor of the attached chamber. */
float CorrFactor; /**< Correction factor of the attached chamber. */
float Offset; /**< Offset (e.g. leakage current) of the ionisation chamber expressed in the measurment unit (romulus_params_t#MeasUnit) [-1e-3, 1e-3]. */
int32_t DefaultPrefix; /**< Prefix of to be used for the measurements (see ROMULUS_DATA_DEFAULT_PREFIX_NANO or more generally ROMULUS_DATA_DEFAULT_PREFIX_*) */
float WeightCoeff; /**< Coefficient for the exponential averaging algorithm (Avg(t) = WeightCoeff * Meas(t) + (1 - WeightCoeff) * Avg(t-1)) [0.00001,0.99999]. */
float Hysteresis; /**< Factor used to lower or increase threshold for alert or alarm switch-off [0.00, 1.00]. */

float AlertThres; /**< Threshold to trigger an alert. */
float AlarmThres; /**< Threshold to trigger an alarm. */
float SpecialAlarmThres; /**< Threshold to trigger a special alarm. */

float Integral1Thres; /**< Threshold to trigger the integral 1 alarm. */
float Integral2Thres; /**< Threshold to trigger the integral 2 alarm. */

float MinThres; /**< Threshold for the minimum value fault. */
float MaxThres; /**< Threshold for the maximum value fault. */
int32_t CountConfig; /**< Comments to be done */
int32_t CAUSerial; /**< Serial of the CAU (read-only). */
int32_t MSID; /**< Measurement ID of the device. */

int32_t SetHighVoltage; /**< High voltage value to ionise the chamber (if this value or romulus_params_t#SetHVControlVoltage is taken depends on the value of romulus_params_t#DetType) [-2000, 0] V. */
float SetHVControlVoltage; /**< Low voltage value to control an external high voltage module (if this value or romulus_params_t#SetHighVoltage is taken depends on the value of romulus_params_t#DetType) [0.0, 5.0] V. */

int32_t AvgTimeFactor; /**< AvgTimeFactor Comments to be done */

float WeightVector1; /**< WeightVector 1 for the weighted moving average averaging algorithm. */
float WeightVector2; /**< WeightVector 2 for the weighted moving average averaging algorithm. */
float WeightVector3; /**< WeightVector 3 for the weighted moving average averaging algorithm. */
float WeightVector4; /**< WeightVector 4 for the weighted moving average averaging algorithm. */
float WeightVector5; /**< WeightVector 5 for the weighted moving average averaging algorithm. */
float WeightVector6; /**< WeightVector 6 for the weighted moving average averaging algorithm. */
float WeightVector7; /**< WeightVector 7 for the weighted moving average averaging algorithm. */
float WeightVector8; /**< WeightVector 8 for the weighted moving average averaging algorithm. */
float WeightVector9; /**< WeightVector 9 for the weighted moving average averaging algorithm. */
float WeightVector10; /**< WeightVector 10 for the weighted moving average averaging algorithm. */

int32_t SimMatrixOutput; /**< Set a certain "output" to a certain value to be simulated with values of the form of ROMULUS_DATA_SIM_MATRIX_OUTPUT* (the simulation value is only applied if the corresponding bit in romulus_params_t#MatrixOutputSelect is set as well). */
int32_t MatrixOutputSelect; /**< Chose a certain "output" to be simulated (value taken from the corresponding bit in romulus_params_t#SimMatrixOutput) by setting the corresponing bit of the form of ROMULUS_DATA_SIM_MATRIX_OUTPUT*. */
int32_t SimMatrixInput; /**< Set a certain "input" to a certain value to be simulated with values of the form of ROMULUS_DATA_SIM_MATRIX_INPUT* (the simulation value is only applied if the corresponding bit in romulus_params_t#MatrixInputSelect is set as well). */
int32_t MatrixInputSelect; /**< Chose a certain "input" to be simulated (value taken from the corresponding bit in romulus_params_t#SimMatrixInput) by setting the corresponing bit of the form of ROMULUS_DATA_SIM_MATRIX_INPUT*. */

bool Coefficients[11][40]; /**< Coefficients for the decision matrix (this thing is too complex to explain here, go and find some documentation ;-)). */

uint8_t DetType; /**< Detector type of the device; this value determinies if romulus_params_t#SetHighVoltage or romulus_params_t#SetHVControlVoltage is taken (possible values are defined with ROMULUS_DATA_DETECTOR_TYPE_* defines and romulus_params_t#SetHVControlVoltage is chosen when ROMULUS_DATA_DETECTOR_TYPE_FHT762 is set). */
uint8_t Mode; /**< Mode of the device (can be Off, Maintainance, Measurment, Temperature Calibration; symbols are defined in the for of ROMULUS_DATA_MODE*).  */
uint8_t MeasUnit; /**< Measurment unit: Sv/h when ROMULUS_DATA_MEASUREMENT_UNIT_SV_H, Gr/h when ROMULUS_DATA_MEASUREMENT_UNIT_GY_H. In Off mode, the device is powered, high voltage is supplied but no alarms are triggered and no measurment data is stored or transmitted and the outputs are in the safe state. Maintanance and Measurement mode are identical from the functional point of view (only the tagging is different). The temperature calibration mode is used to force the temperature compensation coefficients to zero (all the rest is like maintainance or measurment). */
uint8_t ConvUnit; /**< Unit of the conversion factor (set by a symbol of the form of ROMULUS_DATA_CONVERSION_UNIT_MeasUnit* and depending on the the actual measurment unit (romulus_params_t#MeasUnit). */
uint8_t AvgAlgorithm; /**< Switch to choose which averaging algorithm to use (symbols defined in the form of ROMULUS_DATA_AVERAGING_ALGORITHM*). */

bool DHCP; /**< True when DHCP is enabled and false if it is not being used. */
bool TimeSyncEnabled; /**< TimeSyncEnabled Comments to be done */

bool AlertOperator; /**< Use to set the comparison romulus_params_t#AlertThres with the measurment value using ROMULUS_DATA_OPERATOR*. */
bool AlarmOperator; /**< Use to set the comparison romulus_params_t#AlarmThres with the measurment value using ROMULUS_DATA_OPERATOR*. */
bool SpecialAlarmOperator; /**< Use to set the comparison romulus_params_t#SpecialAlarmThres with the measurment value using ROMULUS_DATA_OPERATOR*. */
bool AlertLatched; /**< Sets if the alert is latched, e.g. if it needs to be cleared manually when it goes below the threshold. */
bool AlarmLatched; /**< Sets if the alarm is latched, e.g. if it needs to be cleared manually when it goes below the threshold. */
bool SpecialAlarmLatched; /**< Sets if the special alarm is latched, e.g. if it needs to be cleared manually when it goes below the threshold. */
bool AlertReset; /**< Clears a latched alert as soon as it is below its threshold. */
bool AlarmReset; /**< Clears a latched alarm as soon as it is below its threshold. */
bool SpecialAlarmReset; /**< Clears a latched special alarm as soon as it is below its threshold. */

bool Integral1AlarmLatched; /**< Sets is the integral alarm 1 is latched. */
bool Integral2AlarmLatched; /**< Sets is the integral alarm 2 is latched. */
bool Integral1AlarmReset; /**< Resets the integral alarm 1 as soon as the integral goes below the threshold. */
bool Integral2AlarmReset; /**< Resets the integral alarm 2 as soon as the integral goes below the threshold. */

bool Integral1Reset; /**< Resets the integral 1 value. */
bool Integral2Reset; /**< Resets the integral 2 value. */

bool PersistentFaultReset; /**< Resets the persistant fault. */

bool Reboot; /**< Reboot Comments to be done */
bool FlushMemories; /**< FlushMemories Comments to be done */
bool SyncClock; /**< SyncClock Comments to be done */

bool RomulusRstErr; /**< RomulusRstErr Comments to be done */

bool EnableDebugHist; /**< Activate historics generation for internal debugging information. */

} romulus_params_t;
