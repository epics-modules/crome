#!../../bin/linux-x86_64-debug/crome

#- You may have to change crome to something else
#- everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/crome.dbd"
crome_registerRecordDeviceDriver pdbbase

epicsEnvSet(PORT, "Crome")


# drvAsynIPPortConfigure("CromeIpPort", "127.0.0.1:5004", 0, 0, 1)
# drvAsynIPPortConfigure("CromeIpPort", "rp-agm-001.cslab.esss.lu.se:8889", 0, 0, 1)
# cromeConfigure("portname", "IP", "Port", pollTime)

cromeConfigure("$(PORT)", "rp-agm-001.cslab.esss.lu.se", 8888)


## Load record instances
#dbLoadRecords("db/xxx.db","user=saeedhaghtalab")
# dbLoadRecords("db/crome_2.db","P=, R=, PORT=$(PORT), ADDR=0, TIMEOUT=1")
dbLoadRecords("db/crome.db","P=REMS-LAB:, R=RMT-AGM-01:, PORT=$(PORT), ADDR=0, TIMEOUT=1")

cd "${TOP}/iocBoot/${IOC}"
iocInit

## Start any sequence programs
#seq sncxxx,"user=saeedhaghtalab"
