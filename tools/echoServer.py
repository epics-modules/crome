#!/usr/bin/env python

import socket

# TCP_IP = '172.30.4.92'
TCP_IP = '127.0.0.1'
TCP_PORT = 5004
BUFFER_SIZE = 100  # Normally 1024, but we want fast response

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((TCP_IP, TCP_PORT))
s.listen(1)
print 'listening to', TCP_IP,':',TCP_PORT
conn, addr = s.accept()
print 'Connected to',addr
while 1:
    data = conn.recv(BUFFER_SIZE)
    if not data: break
    print "--------------------------"
    print "received data:", len(data), data
    l1 = []
    l2 = []
    i=0
    for d in data:
        l1.append(d)
        l2.append(hex(ord(d)))
        i = i+1
    print(l1)
    print(l2)
    conn.send(data)  # echo
    # conn.send("ECHO - " + data)  # echo
# conn.close()
