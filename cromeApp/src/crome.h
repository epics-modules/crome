#pragma once

#include <map>
#include <string>

// #include <epicsStdio.h>
#include <asynPortDriver.h>
#include <include/romulus_helper_functions.hpp>
extern "C"{
#include <include/romulus_net.h>
}

#define CROME_RTMEAS_TimeStamp_String           "CROME.RTMEAS.TimeStamp"
#define CROME_RTMEAS_TimeStamp_Epoch_String     "CROME.RTMEAS.TimeStampEpoch"
#define CROME_RTMEAS_ElapsedTimeInt1_String     "CROME.RTMEAS.ElapsedTimeInt1"
#define CROME_RTMEAS_ElapsedTimeInt2_String     "CROME.RTMEAS.ElapsedTimeInt2"
#define CROME_RTMEAS_MSID_String                "CROME.RTMEAS.MSID"
#define CROME_RTMEAS_RawValue_String            "CROME.RTMEAS.RawValue"
#define CROME_RTMEAS_BaseValue_String           "CROME.RTMEAS.BaseValue"
#define CROME_RTMEAS_AvgValue_String            "CROME.RTMEAS.AvgValue"
#define CROME_RTMEAS_Integral1_String           "CROME.RTMEAS.Integral1"
#define CROME_RTMEAS_Integral2_String           "CROME.RTMEAS.Integral2"
#define CROME_RTMEAS_MinValue_String            "CROME.RTMEAS.MinValue"
#define CROME_RTMEAS_MaxValue_String            "CROME.RTMEAS.MaxValue"
#define CROME_RTMEAS_Temperature_String         "CROME.RTMEAS.Temperature"
#define CROME_RTMEAS_Temperature2_String        "CROME.RTMEAS.Temperature2"
#define CROME_RTMEAS_CaseTemp_String            "CROME.RTMEAS.CaseTemp"
#define CROME_RTMEAS_Humidity_String            "CROME.RTMEAS.Humidity"
#define CROME_RTMEAS_MeasUnit_String            "CROME.RTMEAS.MeasUnit"
#define CROME_RTMEAS_Mode_String                "CROME.RTMEAS.Mode"
#define CROME_RTMEAS_Alert_String               "CROME.RTMEAS.Alert"
#define CROME_RTMEAS_Alarm_String               "CROME.RTMEAS.Alarm"
#define CROME_RTMEAS_SpecialAlarm_String        "CROME.RTMEAS.SpecialAlarm"
#define CROME_RTMEAS_MinorFault_String          "CROME.RTMEAS.MinorFault"
#define CROME_RTMEAS_MajorFault_String          "CROME.RTMEAS.MajorFault"
#define CROME_RTMEAS_PersistentFault_String     "CROME.RTMEAS.PersistentFault"
#define CROME_RTMEAS_SensorConnection_String    "CROME.RTMEAS.SensorConnection"
#define CROME_RTMEAS_PowerSupply_String         "CROME.RTMEAS.PowerSupply"
#define CROME_RTMEAS_Battery_String             "CROME.RTMEAS.Battery"
#define CROME_RTMEAS_Charger_String             "CROME.RTMEAS.Charger"
#define CROME_RTMEAS_Input1_String              "CROME.RTMEAS.Input1"
#define CROME_RTMEAS_Input2_String              "CROME.RTMEAS.Input2"
#define CROME_RTMEAS_Input3_String              "CROME.RTMEAS.Input3"
#define CROME_RTMEAS_Input4_String              "CROME.RTMEAS.Input4"
#define CROME_RTMEAS_Output1_String             "CROME.RTMEAS.Output1"
#define CROME_RTMEAS_Output2_String             "CROME.RTMEAS.Output2"
#define CROME_RTMEAS_Output3_String             "CROME.RTMEAS.Output3"
#define CROME_RTMEAS_Output4_String             "CROME.RTMEAS.Output4"

#define CromeStatusMessageString                "DriverStatusMessage"		/**< (asynOctet,	r/o) Status message */

#define BUF_SIZE   1500
#define MAX_TIMESTAMO_STRING_LENGTH 24

class crome : public asynPortDriver {
public:
    crome(const char *portName, const char *deviceIP, const int devicePort);
    void cromePoller();

protected:
    int CROME_RTMEAS_TimeStamp; /**< Timestamp String of the specific sample measured in miliseconds since POSIX epoch. */
    int CROME_RTMEAS_TimeStamp_Epoch; /**< Timestamp of the specific sample measured in miliseconds since POSIX epoch. */
    int CROME_RTMEAS_ElapsedTimeInt1; /**< Elapsed time of integral one. The unit is the same as romulus_params_t#Integral1Time and it is always bigger or equal to 0 and smaller than romulus_params_t#Integral1Time. */
    int CROME_RTMEAS_ElapsedTimeInt2; /**< Elapsed time of integral two. The unit is the same as romulus_params_t#Integral2Time and it is always bigger or equal to 0 and smaller than romulus_params_t#Integral2Time. */
    int CROME_RTMEAS_MSID; /**< Measurment ID where the sample was taken (specified by romulus_params_t#MSID). */
    int CROME_RTMEAS_RawValue; /**< Measured signal in amperes. */
    int CROME_RTMEAS_BaseValue; /**< Measured dose rate. */
    int CROME_RTMEAS_AvgValue; /**< Average value of the dose which is calculated with the algorithm as specified by romulus_params_t#AvgAlgorithm. */
    int CROME_RTMEAS_Integral1; /**< Integral of the dose rate over a specified time (set by romulus_params_t#Integral1Time). */
    int CROME_RTMEAS_Integral2; /**< Integral of the dose rate over a specified time (set by romulus_params_t#Integral2Time). */
    int CROME_RTMEAS_MinValue; /**< Base values filtered with an exponential filter (specified by romulus_params_t#MinTimeCoeff). */
    int CROME_RTMEAS_MaxValue; /**< Base values filtered with an exponential filter (specified by romulus_params_t#MaxTimeCoeff). */
    int CROME_RTMEAS_Temperature; /**< Temperature (in °C) measured every romulus_params_t#THTime inside the measurment device close to the ionisation chamber. */
    int CROME_RTMEAS_Temperature2; /**< Temperature in °C measured every romulus_params_t#THTime inside the device by another sensor than for romulus_hist_data_t#Temperature. */
    int CROME_RTMEAS_CaseTemp; /**< Temperature of the case (measured every romulus_params_t#THTime in °C). */
    int CROME_RTMEAS_Humidity; /**< Relative humidity measured every romulus_params_t#THTime inside the measurment device. */
    int CROME_RTMEAS_MeasUnit; /**< Measurment unit: Sv/h when 0x01, Gr/h when 0x02 and set by romulus_params_t#MeasUnit. */
    int CROME_RTMEAS_Mode; /**< Mode of the measurment device. For more information, see romulus_params_t#Mode. */
    int CROME_RTMEAS_Alert; /**< Defines if there was an alert. */
    int CROME_RTMEAS_Alarm; /**< Defines if there was an alarm. */
    int CROME_RTMEAS_SpecialAlarm; /**< Defines if there was a special alarm. */
    int CROME_RTMEAS_MinorFault; /**< Defines if there was a minor fault. */
    int CROME_RTMEAS_MajorFault; /**< Defines if there was a major fault. */
    int CROME_RTMEAS_PersistentFault; /**< Defines if there was a persistant fault. */
    int CROME_RTMEAS_SensorConnection; /**< True when there is a chamber attached and false if not. */
    int CROME_RTMEAS_PowerSupply; /**< True when the powersupply (CUPS) is ok and false if there is an issue. */
    int CROME_RTMEAS_Battery; /**< True if the battery is ok and charged and false if there is an issue with the battery. */
    int CROME_RTMEAS_Charger; /**< Charger Comments to be done */
    int CROME_RTMEAS_Input1; /**< Input1 state. */
    int CROME_RTMEAS_Input2; /**< Input2 state. */
    int CROME_RTMEAS_Input3; /**< Input3 state. */
    int CROME_RTMEAS_Input4; /**< Input4 state. */
    int CROME_RTMEAS_Output1; /**< Output1 state. */
    int CROME_RTMEAS_Output2; /**< Output2 state. */
    int CROME_RTMEAS_Output3; /**< Output3 state. */
    int CROME_RTMEAS_Output4; /**< Output4 state. */

    int P_CromeStatusMessage;

private:
    const int mdevicePort;
    char mTimeStampStr[MAX_TIMESTAMO_STRING_LENGTH];
    epicsTimeStamp timestamp;

	romulus_socket_t socket;
	uint16_t seq_id;

/////////////////////////////////////////////////

};
