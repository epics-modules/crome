#include <iocsh.h>
#include <epicsExport.h>
#include <epicsThread.h>
#include <dbAccess.h>       // interruptAccept
#include <algorithm>        // std::find_if
#include <cstring>
#include <epicsTime.h>      // timestamps

#include "crome.h"

static const char *driverName = "Crome";
void cromePoller(void *drvPvt);

crome::crome(const char *portName, const char *deviceIP, const int devicePort)
    :mdevicePort(devicePort), asynPortDriver(portName,
                    1, /* maxAddr */ 
                    asynInt32Mask | asynInt64Mask | asynFloat64Mask | asynOctetMask | asynDrvUserMask, /* Interface mask */
                    asynInt32Mask | asynInt64Mask | asynFloat64Mask | asynOctetMask,  /* Interrupt mask */
                    0, /* asynFlags.  This driver does not block and is not multi-device */
                    1, /* Autoconnect */
                    0, /* Default priority */
                    0) /* Default stack size*/
{
    
    createParam(CROME_RTMEAS_TimeStamp_String,          asynParamOctet,     &CROME_RTMEAS_TimeStamp);
    createParam(CROME_RTMEAS_TimeStamp_Epoch_String,    asynParamInt64,     &CROME_RTMEAS_TimeStamp_Epoch);
    createParam(CROME_RTMEAS_ElapsedTimeInt1_String,    asynParamInt32,     &CROME_RTMEAS_ElapsedTimeInt1);
    createParam(CROME_RTMEAS_ElapsedTimeInt2_String,    asynParamInt32,     &CROME_RTMEAS_ElapsedTimeInt2);
    createParam(CROME_RTMEAS_MSID_String,               asynParamInt32,     &CROME_RTMEAS_MSID);
    createParam(CROME_RTMEAS_RawValue_String,           asynParamFloat64,   &CROME_RTMEAS_RawValue);
    createParam(CROME_RTMEAS_BaseValue_String,          asynParamFloat64,   &CROME_RTMEAS_BaseValue);
    createParam(CROME_RTMEAS_AvgValue_String,           asynParamFloat64,   &CROME_RTMEAS_AvgValue);
    createParam(CROME_RTMEAS_Integral1_String,          asynParamFloat64,   &CROME_RTMEAS_Integral1);
    createParam(CROME_RTMEAS_Integral2_String,          asynParamFloat64,   &CROME_RTMEAS_Integral2);
    createParam(CROME_RTMEAS_MinValue_String,           asynParamFloat64,   &CROME_RTMEAS_MinValue);
    createParam(CROME_RTMEAS_MaxValue_String,           asynParamFloat64,   &CROME_RTMEAS_MaxValue);
    createParam(CROME_RTMEAS_Temperature_String,        asynParamFloat64,   &CROME_RTMEAS_Temperature);
    createParam(CROME_RTMEAS_Temperature2_String,       asynParamFloat64,   &CROME_RTMEAS_Temperature2);
    createParam(CROME_RTMEAS_CaseTemp_String,           asynParamFloat64,   &CROME_RTMEAS_CaseTemp);
    createParam(CROME_RTMEAS_Humidity_String,           asynParamFloat64,   &CROME_RTMEAS_Humidity);
    createParam(CROME_RTMEAS_MeasUnit_String,           asynParamInt32,     &CROME_RTMEAS_MeasUnit);
    createParam(CROME_RTMEAS_Mode_String,               asynParamInt32,     &CROME_RTMEAS_Mode);
    createParam(CROME_RTMEAS_Alert_String,              asynParamInt32,     &CROME_RTMEAS_Alert);
    createParam(CROME_RTMEAS_Alarm_String,              asynParamInt32,     &CROME_RTMEAS_Alarm);
    createParam(CROME_RTMEAS_SpecialAlarm_String,       asynParamInt32,     &CROME_RTMEAS_SpecialAlarm);
    createParam(CROME_RTMEAS_MinorFault_String,         asynParamInt32,     &CROME_RTMEAS_MinorFault);
    createParam(CROME_RTMEAS_MajorFault_String,         asynParamInt32,     &CROME_RTMEAS_MajorFault);
    createParam(CROME_RTMEAS_PersistentFault_String,    asynParamInt32,     &CROME_RTMEAS_PersistentFault);
    createParam(CROME_RTMEAS_SensorConnection_String,   asynParamInt32,     &CROME_RTMEAS_SensorConnection);
    createParam(CROME_RTMEAS_PowerSupply_String,        asynParamInt32,     &CROME_RTMEAS_PowerSupply);
    createParam(CROME_RTMEAS_Battery_String,            asynParamInt32,     &CROME_RTMEAS_Battery);
    createParam(CROME_RTMEAS_Charger_String,            asynParamInt32,     &CROME_RTMEAS_Charger);
    createParam(CROME_RTMEAS_Input1_String,             asynParamInt32,     &CROME_RTMEAS_Input1);
    createParam(CROME_RTMEAS_Input2_String,             asynParamInt32,     &CROME_RTMEAS_Input2);
    createParam(CROME_RTMEAS_Input3_String,             asynParamInt32,     &CROME_RTMEAS_Input3);
    createParam(CROME_RTMEAS_Input4_String,             asynParamInt32,     &CROME_RTMEAS_Input4);
    createParam(CROME_RTMEAS_Output1_String,            asynParamInt32,     &CROME_RTMEAS_Output1);
    createParam(CROME_RTMEAS_Output2_String,            asynParamInt32,     &CROME_RTMEAS_Output2);
    createParam(CROME_RTMEAS_Output3_String,            asynParamInt32,     &CROME_RTMEAS_Output3);
    createParam(CROME_RTMEAS_Output4_String,            asynParamInt32,     &CROME_RTMEAS_Output4);

    createParam(CromeStatusMessageString ,              asynParamOctet,     &P_CromeStatusMessage);
	setStringParam(P_CromeStatusMessage, "");

    int romulus_init_status = romulus_init(driverName);
    /* TODO: handle romulus_init_status */
    seq_id = romulus_frame_generate_sequence_id(&seq_id);
    socket = romulus_net_connect(deviceIP, devicePort);

    status = (asynStatus)(epicsThreadCreate("cromePoller", 
                      epicsThreadPriorityLow,
                      epicsThreadGetStackSize(epicsThreadStackSmall),
                      (EPICSTHREADFUNC)::cromePoller,   // Function that implements the thread
                      this) == NULL);                   // Parameter passed to cromePoller

}

void cromePoller(void *drvPvt){
    crome *pPvt = (crome *)drvPvt;
    pPvt->cromePoller();

}

void crome::cromePoller()
{
    /* Wait for iocInit to finish */
    while (!interruptAccept)
    {                
        epicsThreadSleep(0.1);
    }

    romulus_frame_t* frame_send = romulus_frame_create_request(seq_id, ROMULUS_COMMAND_CODE_RTMEAS_REQUEST);
    ssize_t frame_send_size = romulus_frame_get_size(frame_send);
    
    ssize_t stat = romulus_net_send(frame_send, frame_send_size, socket, true);
    /* TODO: Add error handling here */
    bool socket_error;

    romulus_rtmeas_t rtmeas_struct;

    const romulus_frame_t* frame_write_struct_status;
    romulus_frame_t* frame_recv;

    while (socket != -1)
    {
        lock();

        frame_recv = romulus_net_recv(NULL, socket, true, &socket_error);
        /* TODO: handle socket_error */
        if (!frame_recv){
            // Print error
            continue;
        }
        frame_write_struct_status = romulus_frame_rtmeas_write_frame_to_struct(frame_recv, &rtmeas_struct);

        /* Write the epoch TS from the device directly to parameter library */
        setInteger64Param(CROME_RTMEAS_TimeStamp_Epoch,       rtmeas_struct.TimeStamp);

        /* Convert the device timestamp to epics time stamp. */
        timestamp.secPastEpoch = rtmeas_struct.TimeStamp / 1000;    // millisecond to second
        timestamp.secPastEpoch -= POSIX_TIME_AT_EPICS_EPOCH;        // convert to epics epoch. Note in epics TS epoch starts from 1990
        timestamp.nsec = rtmeas_struct.TimeStamp % 1000 * 1.e6 ;    // Calculate nanosec from device epoch in millisec
        setTimeStamp(&timestamp);

        /* Transform the epoch timestamp to string */
        struct tm tm;
        unsigned long msec;
        epicsTimeToTM(&tm, &msec, &timestamp);
        msec /= 1000000;             // nsec to msec
        snprintf(mTimeStampStr, sizeof(mTimeStampStr), "%04d-%02d-%02d %02d:%02d:%02d.%03lu",
        tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec, msec);

        setStringParam(CROME_RTMEAS_TimeStamp,          mTimeStampStr);
        
        setIntegerParam(CROME_RTMEAS_ElapsedTimeInt1,   rtmeas_struct.ElapsedTimeInt1);
        setIntegerParam(CROME_RTMEAS_ElapsedTimeInt2,   rtmeas_struct.ElapsedTimeInt2);
        setIntegerParam(CROME_RTMEAS_MSID,              rtmeas_struct.MSID);
        setDoubleParam(CROME_RTMEAS_RawValue,           rtmeas_struct.RawValue);
        setDoubleParam(CROME_RTMEAS_BaseValue,          rtmeas_struct.BaseValue);
        setDoubleParam(CROME_RTMEAS_AvgValue,           rtmeas_struct.AvgValue);
        setDoubleParam(CROME_RTMEAS_Integral1,          rtmeas_struct.Integral1);
        setDoubleParam(CROME_RTMEAS_Integral2,          rtmeas_struct.Integral2);
        setDoubleParam(CROME_RTMEAS_MinValue,           rtmeas_struct.MinValue);
        setDoubleParam(CROME_RTMEAS_MaxValue,           rtmeas_struct.MaxValue);
        setDoubleParam(CROME_RTMEAS_Temperature,        rtmeas_struct.Temperature);
        setDoubleParam(CROME_RTMEAS_Temperature2,       rtmeas_struct.Temperature2);
        setDoubleParam(CROME_RTMEAS_CaseTemp,           rtmeas_struct.CaseTemp);
        setDoubleParam(CROME_RTMEAS_Humidity,           rtmeas_struct.Humidity);
        setIntegerParam(CROME_RTMEAS_MeasUnit,          rtmeas_struct.MeasUnit);
        setIntegerParam(CROME_RTMEAS_Mode,              rtmeas_struct.Mode);
        setIntegerParam(CROME_RTMEAS_Alert,             rtmeas_struct.Alert);
        setIntegerParam(CROME_RTMEAS_Alarm,             rtmeas_struct.Alarm);
        setIntegerParam(CROME_RTMEAS_SpecialAlarm,      rtmeas_struct.SpecialAlarm);
        setIntegerParam(CROME_RTMEAS_MinorFault,        rtmeas_struct.MinorFault);
        setIntegerParam(CROME_RTMEAS_MajorFault,        rtmeas_struct.MajorFault);
        setIntegerParam(CROME_RTMEAS_PersistentFault,   rtmeas_struct.PersistentFault);
        setIntegerParam(CROME_RTMEAS_SensorConnection,  rtmeas_struct.SensorConnection);
        setIntegerParam(CROME_RTMEAS_PowerSupply,       rtmeas_struct.PowerSupply);
        setIntegerParam(CROME_RTMEAS_Battery,           rtmeas_struct.Battery);
        setIntegerParam(CROME_RTMEAS_Charger,           rtmeas_struct.Charger);
        setIntegerParam(CROME_RTMEAS_Input1,            rtmeas_struct.Input1);
        setIntegerParam(CROME_RTMEAS_Input2,            rtmeas_struct.Input2);
        setIntegerParam(CROME_RTMEAS_Input3,            rtmeas_struct.Input3);
        setIntegerParam(CROME_RTMEAS_Input4,            rtmeas_struct.Input4);
        setIntegerParam(CROME_RTMEAS_Output1,           rtmeas_struct.Output1);
        setIntegerParam(CROME_RTMEAS_Output2,           rtmeas_struct.Output2);
        setIntegerParam(CROME_RTMEAS_Output3,           rtmeas_struct.Output3);
        setIntegerParam(CROME_RTMEAS_Output4,           rtmeas_struct.Output4);

        callParamCallbacks();

        unlock();
    }
    
}


extern "C" {
/** EPICS iocsh callable function to call constructor for the crome class. */

int cromeConfigure(const char *portName, const char *deviceIP, const int devicePort)
{
    new crome(portName, deviceIP, devicePort);
    return(asynSuccess);
}

/* EPICS iocsh shell commands */

static const iocshArg initArg0 = {"portName",   iocshArgString};
static const iocshArg initArg1 = {"deviceIP",   iocshArgString};
static const iocshArg initArg2 = {"devicePort", iocshArgInt};
// static const iocshArg initArg2 = {"pollTime",   iocshArgInt};

static const iocshArg * const initArgs[] = {&initArg0, &initArg1, &initArg2};
static const iocshFuncDef initFuncDef = {"cromeConfigure",3,initArgs};
static void initCallFunc(const iocshArgBuf *args)
{
    cromeConfigure(args[0].sval, args[1].sval, args[2].ival);
}

void cromeRegister(void)
{
    iocshRegister(&initFuncDef,initCallFunc);
}

epicsExportRegistrar(cromeRegister);

}
